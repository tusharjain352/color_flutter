


import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/widget_theme_data/custon_button_theme.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Alert {

  static void show(message){
    Fluttertoast.showToast(
      msg: message,
    );
  }


  static void showDialogue(BuildContext context,String message){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Alert"),
          content: Text(message),
          actions: [

            TextButtonTheme(
              data: CustomButtonTheme.text,
              child: TextButton(onPressed: (){
                MyNavigator.pop(context);
              }, child: const Text("OK")),
            ),
          ],
        );
      },
    );
  }

}