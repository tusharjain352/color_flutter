



import 'package:url_launcher/url_launcher.dart';

class MyUrlLauncher {

  static Future<void> launchInBrowser(String url) async {
    Uri uri=Uri.parse(url);
    if (!await launchUrl(
      uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $uri';
    }
  }


  static Future<void> launchMail(String mailId) async {

    final Uri uri = Uri(
      scheme: 'mailto',
      path: mailId,
    );
    if (!await launchUrl(
      uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $uri';
    }
  }

  static Future<void> launchSMS({
    required String number,
    String? body,
}) async {
    final Uri uri = Uri.parse('sms:$number?body=$body');
    if (!await launchUrl(
      uri,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $uri';
    }
  }


}