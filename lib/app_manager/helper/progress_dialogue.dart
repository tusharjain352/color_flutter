import 'package:flutter/material.dart';
import 'package:colurama/app_manager/assets/assets.dart';

late BuildContext currentContext;

class PD {
  static show(
    context, {
    required String message,
  }) async {
    _showProgress(context, message);
  }

  static bool hide() {
    try {
      Navigator.pop(currentContext);
      return true;
    } catch (e) {
      return false;
    }
  }
}

_showProgress(
  context,
  message,
) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext dialogContext) {
      currentContext = dialogContext;
      return WillPopScope(
        onWillPop: () {
          PD.hide();
          return Future.value(false);
        },
        child: ProgressView(
          message: message,
        ),
      );
    },
  );
}

class ProgressView extends StatelessWidget {
  final String message;

  const ProgressView({super.key, required this.message});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          color: Colors.black54,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SizedBox(
                            height: 100,
                            child: Container(
                              decoration: const BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(Assets.images.progressJson),
                              ),
                            )),
                      ),
                      // Text(message,
                      //     textAlign: TextAlign.center,
                      //     style: textTheme.bodyMedium!.copyWith(
                      //       color: AppColor.white
                      //     )
                      // ),
                      // factsDialogue(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
