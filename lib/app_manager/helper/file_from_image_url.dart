

import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';


Future<File> fileFromImageUrl(String url) async {
  final response = await http.get(Uri.parse(url));
      final documentDirectory = Platform.isIOS? await getApplicationSupportDirectory(): await getApplicationDocumentsDirectory();
  final file = File('${documentDirectory.path}images.png');
  file.writeAsBytesSync(response.bodyBytes);
  return file;
}