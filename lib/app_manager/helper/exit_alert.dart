





import 'dart:io';

import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/widget_theme_data/custon_button_theme.dart';
import 'package:flutter/material.dart';



void showExitDialogue(BuildContext context){
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text("Exit App"),
        content: const Text("Are you sure you want to close the app?"),
        actions: [
          TextButtonTheme(
            data: CustomButtonTheme.text,
            child: TextButton(onPressed: (){
              MyNavigator.pop(context);
            }, child: const Text("CANCEL")),
          ),
          TextButtonTheme(
            data: CustomButtonTheme.text,
            child: TextButton(onPressed: (){
              exit(0);
            }, child: const Text("OK")),
          ),
        ],
      );
    },
  );
}