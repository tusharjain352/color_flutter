
import 'package:flutter/material.dart';
import 'package:material_color_generator/material_color_generator.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:colurama/app_manager/constant/storage_constant.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/app_manager/theme/widget_theme_data/custom_text_field_theme.dart';
import 'package:colurama/app_manager/theme/widget_theme_data/custon_button_theme.dart';

enum ThemeType {
  light,
  dark
}

class ThemeProvider with ChangeNotifier {
  bool isLightTheme;
  ThemeProvider({
    this.isLightTheme=true,
  });


  static ThemeProvider of(BuildContext context)=>Provider.of<ThemeProvider>(context,listen: false);

  ThemeData get getThemeData => isLightTheme ? lightTheme() : darkTheme();
  set setThemeData(ThemeType val) {
    isLightTheme=(val==ThemeType.light);
    _storeTheme(isLightTheme);
    notifyListeners();
  }

  void _storeTheme(bool value) async{
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool(StorageConstant.isLightTheme, value);
  }

  Future<bool> retrieveStoredTheme() async{
    var prefs = await SharedPreferences.getInstance();
    return prefs.getBool(StorageConstant.isLightTheme)??true;
  }

  // bool get getSystemIsLight=>SchedulerBinding.instance.window.platformBrightness == Brightness.light;



  // define values here



  // for light theme
  static const String _fontFamily="HelveticaNeue";
      //"Montserrat";
  static const Color _primaryColor=AppColor.primaryColor;
  static final Color _secondaryColor=AppColor.secondaryColor;
  static  final Color _backgroundColor=AppColor.backgroundColor;
  static final Color _scaffoldBackgroundColor=_backgroundColor;
  static const typeTheme = Typography.whiteMountainView;
  static    const TextTheme _textTheme=TextTheme(
  );
  static const ListTileThemeData _listTileThemeData=ListTileThemeData(
    selectedColor: _primaryColor,
  );


  static final TextButtonThemeData _textButtonThemeData=CustomButtonTheme.primary;
  static final InputDecorationTheme _inputDecorationTheme=CustomTextFieldTheme.primary;


  // for dark theme
  static const Color _primaryColorD=Colors.black54;

  static const AppBarTheme _appBarTheme=AppBarTheme(
      backgroundColor: AppColor.white,
      titleTextStyle: TextStyle(
        color: AppColor.primaryColor,
        fontWeight: FontWeight.w500
      ),
      centerTitle: true,
      elevation: 0,
  );



  static ThemeData lightTheme(){
    return ThemeData(
      platform: TargetPlatform.iOS,
      appBarTheme: _appBarTheme,
      canvasColor: Colors.white,
      brightness: Brightness.light,
      fontFamily: _fontFamily,
      colorScheme: ColorScheme.fromSwatch().copyWith(
        primary: _primaryColor,
        secondary: _secondaryColor,
        background: _backgroundColor,
      ),
      primarySwatch: generateMaterialColor(color: _primaryColor),
      primaryColor: _primaryColor,
      secondaryHeaderColor: _secondaryColor,
      textTheme:  _textTheme,
      listTileTheme: _listTileThemeData,
      scaffoldBackgroundColor: _scaffoldBackgroundColor,



      textButtonTheme: _textButtonThemeData,
      inputDecorationTheme: _inputDecorationTheme,
    );
  }



  static ThemeData darkTheme(){
    return ThemeData(
        platform: TargetPlatform.iOS,
        appBarTheme: _appBarTheme,
      brightness: Brightness.dark,
      fontFamily: _fontFamily,
      colorScheme: ColorScheme.fromSwatch().copyWith(
        primary: _primaryColorD,
        secondary: _secondaryColor,
        background: _backgroundColor,
      ),
      primarySwatch: generateMaterialColor(color: _primaryColorD),
      primaryColor: _primaryColorD,
      secondaryHeaderColor: _secondaryColor,

      textTheme:  _textTheme,
      listTileTheme: _listTileThemeData,
      scaffoldBackgroundColor: _scaffoldBackgroundColor,

      textButtonTheme: _textButtonThemeData,
        inputDecorationTheme: _inputDecorationTheme
    );
  }







}










