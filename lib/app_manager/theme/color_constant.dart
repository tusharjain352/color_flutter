



import 'package:flutter/material.dart';
import 'package:colurama/app_manager/extension/color_extention.dart';

class AppColor {

  static const Color primaryColor=Colors.blueAccent;
  static final Color secondaryColor="#2d2cca".toColor();
  static final Color maroon="#8d1b1b".toColor();

  static  Color backgroundColor="#f7f7f7".toColor();


  static final Color greyLight=Colors.grey.shade200;
  static final Color greyDark=Colors.grey.shade600;
  static const Color black=Colors.black;
  static const Color white=Colors.white;



  static const Color tableBgGreen = Color.fromRGBO(77,193,150,1);
  static const Color tableBgYellow = Color.fromRGBO(255,255,102, 1);
  static const Color tableBgAmber = Color.fromRGBO(255,153,51,1);
  static const Color tableBgRed = Color.fromRGBO(255,102,102,1);

  static final Color purpleCustomShade="#ded5e4".toColor();
  static final Color greenCustomShade="#d3eadd".toColor();

}