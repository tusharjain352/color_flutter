import 'package:flutter/material.dart';

class CustomTextFieldTheme {



  static InputDecorationTheme primary=   const InputDecorationTheme(
    isDense: true,
    contentPadding: EdgeInsets.fromLTRB(0,9,0,9,),
    fillColor: Colors.transparent,
    filled: true,
    border: UnderlineInputBorder(
      borderSide: BorderSide(
        color: Colors.black
      )
    ),
  );


  static InputDecorationTheme secondary=   InputDecorationTheme(
    isDense: true,
    contentPadding: const EdgeInsets.all(2),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(3.0),
    ),
  );

}