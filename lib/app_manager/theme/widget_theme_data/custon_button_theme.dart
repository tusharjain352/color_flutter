





import 'package:flutter/material.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';

class CustomButtonTheme {



  static TextButtonThemeData primary=TextButtonThemeData(
    style: TextButton.styleFrom(
        backgroundColor: AppColor.primaryColor,
        foregroundColor: Colors.white,
        textStyle: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w500
        ),
        shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        side: const BorderSide(
            color: AppColor.primaryColor
        ),

      elevation: 5
    ),
  );

  static TextButtonThemeData text=TextButtonThemeData(
    style: TextButton.styleFrom(
        backgroundColor: Colors.transparent,
        foregroundColor: AppColor.primaryColor,
        textStyle: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400
        ),

    ),
  );


  static TextButtonThemeData secondary=TextButtonThemeData(
    style: TextButton.styleFrom(
        backgroundColor: AppColor.primaryColor,
        foregroundColor: Colors.white,
        textStyle: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w500
        ),
        shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0),
        ),
        side: const BorderSide(
            color: AppColor.primaryColor
        )
    ),
  );


  static TextButtonThemeData whiteSecondaryButton=TextButtonThemeData(
    style: TextButton.styleFrom(
        backgroundColor: AppColor.white,
        foregroundColor: AppColor.black,
        textStyle: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w500
        ),
        shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0),
        ),
        side: const BorderSide(
          color: AppColor.primaryColor
        )
    ),
  );

}