




import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:colurama/app_manager/api/api_constant.dart';
import 'package:colurama/app_manager/api/error_alert.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/service/local_storage.dart';

enum ApiType{
  get,
  post,
  rawPost,
  multiPartRequest,
  put,
  rawPut,
  options,
  patch,
  delete,
}



class ApiCallType {
  Map? body;
  String? fileParameter;
  String? filePath;
  ApiType apiType;

  ApiCallType.get() : apiType = ApiType.get;
  ApiCallType.post({
    required this.body
  }) : apiType = ApiType.post;
  ApiCallType.rawPost({
    required this.body
  }) : apiType = ApiType.rawPost;

  ApiCallType.multiPartRequest({
    required this.filePath,
    required this.fileParameter,
    required this.body,
  }) : apiType = ApiType.multiPartRequest;

  ApiCallType.put({
    required this.body
  }) : apiType = ApiType.put;

  ApiCallType.rawPut({
    required this.body
  }) : apiType = ApiType.rawPut;

  ApiCallType.options() : apiType = ApiType.options;

  ApiCallType.patch({
    required this.body
  }) : apiType = ApiType.patch;

  ApiCallType.delete({
    required this.body
  }) : apiType = ApiType.delete;



}



class ApiCall {


  final LocalStorage _localStorage=LocalStorage();


  Future<dynamic> call(context, {
    required String url,
    required ApiCallType apiCallType,
    bool showRetryEvent = false,
    bool token = false,

    String? newBaseUrl,
    bool localStorage=false,
    ValueChanged? onFoundStoredData
  }) async {
    if(ApiConstant.baseUrl==""){
      Alert.show("Add base url in while initiating reuse kit");
    }
    String myUrl = (newBaseUrl ?? ApiConstant.baseUrl) + url;
    String accessToken = "";
  //  UserRepository.of(context).getUser.token.toString();
   // String userId = "";
    Map body = apiCallType.body??{};
    Map<String,String>? header = token?{
      'authorization': accessToken.toString(),
    }:null;
    if(apiCallType.apiType==ApiType.rawPost  || apiCallType.apiType==ApiType.options || apiCallType.apiType==ApiType.rawPut){
      header=header??{};
      header.addAll({
        'Content-Type': 'application/json'
      });
    }


    if(onFoundStoredData!=null){
      var storedData=(await _localStorage.fetchData(key: url));
      if(storedData!=null){
        onFoundStoredData(storedData);
      }

    }

    http.Response? response;
    if (kDebugMode) {
      print("Api call at ${DateTime.now()}");
      print("Type: ${apiCallType.apiType.name.toString()}");
      if(header!=null){
        print("Header: $header");
      }
      print("URL: $myUrl");
      print("BODY: $body");
    }


    try {
      switch (apiCallType.apiType) {
        case ApiType.post:
          response = await http.post(
              Uri.parse(myUrl),
              body: body,
              headers: header
          );
          break;

        case ApiType.get:
          response = await http.get(
              Uri.parse(myUrl),
              headers: header
          );
          break;

        case ApiType.rawPost:
          var request = http.Request('POST', Uri.parse(myUrl));
          request.body = json.encode(body);
          request.headers.addAll(header??{
            'Content-Type': 'application/json'
          });
          response= await http.Response.fromStream((await request.send()));
          break;


        case ApiType.multiPartRequest:
          Alert.show("Not implemented");
          // var request = http.Request('POST', Uri.parse(myUrl));
          // request.body = json.encode(body);
          // request.headers.addAll(header??{
          //   'Content-Type': 'application/json'
          // });
          // streamResponse = await request.send();
          break;


        case ApiType.put:
          response = await http.put(
              Uri.parse(myUrl),
              body: body,
              headers: header
          );
          break;
        case ApiType.rawPut:
          var request = http.Request('PUT', Uri.parse(myUrl));
          request.body = json.encode(body);
          request.headers.addAll(header??{
            'Content-Type': 'application/json'
          });
          response= await http.Response.fromStream((await request.send()));
          break;

        case ApiType.options:

          var request = http.Request('OPTIONS', Uri.parse(myUrl));
          request.body = json.encode(body);
          request.headers.addAll(header??{
            'Content-Type': 'application/json'
          });
          response= await http.Response.fromStream((await request.send()));
          break;

        case ApiType.patch:
          response = await http.patch(
              Uri.parse(myUrl),
              body: body,
              headers: header
          );
          break;

        case ApiType.delete:
          response = await http.delete(
              Uri.parse(myUrl),
              body: body,
              headers: header
          );
          break;
        default:
          break;
      }


      if (response != null) {
        var data = await _handleDecodeAndStorage(
          context,
          url: url,
          localStorage: localStorage,
          encodeData: response.body,

        );


        return data;
      }
      else {
        Alert.show("Null response");
        var storedData=(await _localStorage.fetchData(key: url));
        var errorRes=storedData??ApiConstant.cancelResponse;
        return errorRes;
      }
    }
    catch (e) {



      if (showRetryEvent) {
        var retry = await errorAlert(context, 'Alert', e.toString(),
        );
        if (retry) {
          var data = await call(context,
              url: url,
              apiCallType: apiCallType,
              showRetryEvent: showRetryEvent,
              token: token,
              newBaseUrl: newBaseUrl,
              localStorage: localStorage
          );
          return data;
        }
        else {
          var storedData=(await _localStorage.fetchData(key: url));
          var errorRes=storedData??ApiConstant.cancelResponse;
          return errorRes;
        }
      }
      else {
        var storedData=(await _localStorage.fetchData(key: url));
        var errorRes=storedData??ApiConstant.cancelResponse;
        return errorRes;
      }
    }
  }




  Future<dynamic> _handleDecodeAndStorage(
  context,
      {

    required String url,
    required var encodeData,
    required bool localStorage,
  }) async{
    if (kDebugMode) {
      print("Response: $encodeData\n");
    }
    try{


      var decodeData=(await json.decode(encodeData));
      // if((decodeData['message']??"")=="You are not authorized to access this resource."){
      //  // MyNavigator.pushAndRemoveUntil(context, const HomeView());
      // }


      if(localStorage){
        _localStorage.storeData(key: url, data: decodeData);
      }

      return decodeData;
    }
    catch(e){
      var storedData=(await _localStorage.fetchData(key: url));
      var errorRes=storedData??ApiConstant.cancelResponse;
      return errorRes;
    }

  }


}