


import 'package:colurama/app_manager/assets/assets.dart';

class ApiConstant {

  static String baseUrl="https://sit1-gilcolourama.innov.co.in/api";
  static const Map cancelResponse={'type': "failed", 'message': 'Try Again...'};
  static String noInterNetJson=Assets.images.noInternetJson;
  static String progressJson=Assets.images.noInternetJson;
  static String noDataFound=Assets.images.noDataFound;

}