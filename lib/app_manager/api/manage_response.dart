


import 'package:colurama/app_manager/api/api_constant.dart';
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/assets/assets.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';


class ManageResponse extends StatelessWidget {


  final ApiResponse response;
  final Widget child;
  final Function? onPressRetry;
  final bool showRetryOnEmptyData;
  final bool showChildOn;
  final bool showImage;
  final Axis axis;


  const ManageResponse({Key? key, required this.response,
    required this.child,
    this.onPressRetry,
    this.showRetryOnEmptyData=true,
    this.showChildOn=false,
    this.showImage=true,  this.axis=Axis.vertical,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return getWidgetAccordingToResponse(textTheme);
  }


  Widget getWidgetAccordingToResponse(TextTheme textTheme) {


    bool showHorizontal=axis==Axis.horizontal;

    List<Widget> loadingW=[
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: SizedBox(
            height: 65,
            child: Container(
              decoration: const BoxDecoration(
                  color: Colors.white, shape: BoxShape.circle),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(Assets.images.progressJson),
              ),
            )),
      )
    ];

    if(showChildOn){
      return child;
    }
    else {
      switch (response.status) {
        case Status.loading:
          return  Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child:
              showHorizontal?

              Row(
                mainAxisSize: MainAxisSize.min,
                children: loadingW,
              )
                  :Column(
                mainAxisSize: MainAxisSize.min,
                children: loadingW,
              ),
            ),
          );
        case Status.completed:
          return child;
        case Status.error:
          return _retryWidget(textTheme) ;

        case Status.empty:
          return _retryWidget(textTheme) ;
        case Status.initial:
        default:
          return  Container();
      }
    }

  }



  _retryWidget(TextTheme textTheme){
    List<Widget> retryW=[
      showImage?SizedBox(
          height: 100,
          child: Lottie.asset(ApiConstant.noDataFound)):Container(),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: response.message.toString().contains("SocketException")?
        Text("Internet connection issue",style: textTheme.titleMedium)
            :Text(response.message.toString(),style: textTheme.titleMedium,
          maxLines: 2,),
      )
    ];
    bool showHorizontal=axis==Axis.horizontal;

    List<Widget> fullRetryW= [
      showHorizontal?
      Expanded(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: retryW,
        ),
      )
          :Column(
        mainAxisSize: MainAxisSize.min,
        children: retryW,
      ),
      (
          onPressRetry!=null  && (showRetryOnEmptyData || response.status!=Status.empty )
      )? Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: SizedBox(

            child: TextButton(
              onPressed: (){
                onPressRetry!();
              }, child: const Text("RETRY"),),
          ),
        ),
      ):Container()
    ];



    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: showHorizontal?
        Row(
          mainAxisSize: MainAxisSize.min,
          children: fullRetryW,
        )
            :Column(
          mainAxisSize: MainAxisSize.min,
          children: fullRetryW,
        ),
      ),
    );
  }

}

