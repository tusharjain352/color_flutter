



import 'package:flutter/material.dart';
class ColoredSafeArea extends StatelessWidget {
  final Widget child;
  final Color? color;

  const ColoredSafeArea({Key? key, required this.child, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
  //  final theme = Theme.of(context);
    return Container(
      color: color ?? Colors.white,
      child: SafeArea(
        child: child,
      ),
    );
  }
}

