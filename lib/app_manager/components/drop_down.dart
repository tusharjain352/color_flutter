
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';

class MyDropDown<T> extends StatelessWidget {

  final T? value;
  final String hint;
  final bool isExpanded;
  final ValueChanged<T?> onChanged;
  final List<DropdownMenuItem<T>>? items;
  final bool? enableFeedback;

  const MyDropDown({super.key,
    this.value,
    required this.hint,
    this.isExpanded = false,
    required this.onChanged,
    this.items, this.enableFeedback
  });


  @override
  Widget build(BuildContext context) {
    return  Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: AppColor.greyLight,
            width: 2
          )
        ),
      ),
      height: 40,
      child: DropdownButton<T>(
        value: value,
        alignment: AlignmentDirectional.centerStart,
        onChanged: onChanged,
        underline: Container(),
        menuMaxHeight: 300,
          enableFeedback: enableFeedback,
        hint: Padding(
          padding: const EdgeInsets.fromLTRB(10,0,0,0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(hint,
            textAlign: TextAlign.start,),
          ),
        ),

        selectedItemBuilder: (BuildContext context) { //<-- SEE HERE
          return (items??[]).map((e) => e.child).toList()
              .map((Widget value) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(10,0,0,0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: value,
              ),
            );
          }).toList();
        },
        items: items,
        isExpanded: isExpanded,
      ),
    );
  }
}





class MySearchableDropDown<T> extends StatelessWidget {

  final T? value;
  final String hint;
  final ValueChanged<T?> onChanged;
  final List<T> items;
  final FormFieldValidator<T>? validator;
  final DropdownSearchItemAsString<T>? itemAsString;

  const MySearchableDropDown({super.key,
    this.value,
    required this.hint,
    required this.onChanged,
    required this.items,
    this.validator,
    this.itemAsString,
  });


  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    return  Container(
      constraints: const BoxConstraints(
        maxHeight: 30,
      ),
      child: DropdownSearch<T>(
        itemAsString: itemAsString,
        onChanged: onChanged,
        items: items,
        selectedItem: value,
          popupProps:  const PopupProps.menu(
            showSearchBox: true,
              searchFieldProps: TextFieldProps(
                  decoration: InputDecoration(
                    hintText: "Search Here...."
                  )
              )

          ),
          dropdownButtonProps: const DropdownButtonProps(
            padding: EdgeInsets.fromLTRB(8,0,8,0),
              iconSize: 16,
            constraints: BoxConstraints(
              maxHeight: 30,
            )
          ),
          dropdownDecoratorProps: DropDownDecoratorProps(

            dropdownSearchDecoration: InputDecoration(


              hintText: hint
            ).applyDefaults(theme.inputDecorationTheme)
          ),
        validator: validator,


      ),
    );
  }
}




