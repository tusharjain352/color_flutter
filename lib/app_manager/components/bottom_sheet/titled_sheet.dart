



import 'package:flutter/material.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';



class TitledSheet extends StatelessWidget {

  final String title;
  final String? subTitle;
  final Widget child;
  final EdgeInsetsGeometry? bodyPadding;
  final EdgeInsetsGeometry? titlePadding;
  final List<Widget>? action;

  const TitledSheet({Key? key,
  required this.title,
  this.subTitle,
  this.bodyPadding,
  required this.child, this.action, this.titlePadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: const BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.grey
              )
            )
          ),
          child: Padding(
            padding: titlePadding??const EdgeInsets.all(20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset(
                      "assets/images/cross.png",
                      scale: 4,
                    )),
                const SizedBox(width: 20,),
                Expanded(child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    Text(title.toString(),
                        style: theme.textTheme.titleLarge!.copyWith(
                          color: Colors.grey.shade700
                        )),
                    subTitle==null? Container():Text(subTitle.toString(),
                        style: const TextStyle(
                            color: AppColor.black,
                            fontSize: 12,
                            fontWeight: FontWeight.bold
                        ),),
                  ],
                )),
                Row(children: action??[],)
              ],
            ),
          ),
        ),
        Padding(
          padding: bodyPadding??const EdgeInsets.fromLTRB(20,10,20,20),
          child: child,
        )
      ],
    );
  }
}
