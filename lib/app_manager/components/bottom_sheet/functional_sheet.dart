


import 'package:flutter/material.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/app_manager/theme/widget_theme_data/custon_button_theme.dart';






class FunctionalSheet extends StatelessWidget {

  final String message;
  final String buttonName;
  final Function onPressButton;
  final bool showCancelButton;
  final TextAlign? textAlign;

  const FunctionalSheet({Key? key,
    required this.message,
    required this.buttonName,
    required this.onPressButton,
    this.showCancelButton=true,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(20,40,20,20),
          child: Text(message.toString(),
            textAlign: textAlign??TextAlign.center,
            style: const TextStyle(
              color: AppColor.black,
              fontWeight: FontWeight.bold
            ),),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20,20,20,20),
          child: Row(
            children: [

              showCancelButton? Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0,0,20,0),
                  child: TextButtonTheme(
                    data: CustomButtonTheme.whiteSecondaryButton,
                    child: TextButton(
                        onPressed: (){

                          Navigator.pop(context);
                        }, child: const Text("CANCEL"),),
                  ),
                ),
              ):Container(),
              Expanded(
                child: TextButtonTheme(
                  data: CustomButtonTheme.secondary,
                  child: TextButton(
                      onPressed: (){

                    Navigator.pop(context);
                    onPressButton();
                  }, child:  Text(buttonName.toUpperCase()),),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
