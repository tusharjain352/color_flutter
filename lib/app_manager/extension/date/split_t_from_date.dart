

import 'package:flutter/foundation.dart';

extension DateSpliter on String {
  String splitTFromDate() {
    String data="";
    try{
      data=split("T")[0];
    }
    catch(e){
      if (kDebugMode) {
        print(e.toString());
      }
    }
    return data;
  }
}
