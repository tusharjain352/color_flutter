



import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:flutter/material.dart';

class ButtonBorder extends StatelessWidget {
  final Widget child;

  const ButtonBorder({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: AppColor.white,
          border: Border.all(
              color: AppColor.primaryColor
          ),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: AppColor.black.withOpacity(0.4),
              offset: const Offset(0,2),
            )
          ]
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: IntrinsicHeight(
          child: child,
        ),
      ),
    );
  }
}
