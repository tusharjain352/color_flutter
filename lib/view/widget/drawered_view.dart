

import 'dart:io';

import 'package:colurama/app_manager/components/colored_safe_area.dart';
import 'package:colurama/app_manager/helper/exit_alert.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/authentication/user_repository.dart';
import 'package:colurama/model/drawer_option.dart';
import 'package:colurama/routes.dart';
import 'package:colurama/view_model/drawer_view_model.dart';
import 'package:flutter/material.dart';

class DraweredView extends StatefulWidget {
  final String title;
  final Widget child;

  const DraweredView({super.key, required this.title, required this.child});

  @override
  State<DraweredView> createState() => _DraweredViewState();
}

class _DraweredViewState extends State<DraweredView> {
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    Widget endDrawer = Drawer(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                    onTap: () {
                      _key.currentState!.closeEndDrawer();
                    },
                    child: Image.asset(
                      "assets/images/cross.png",
                      scale: 4,
                    )),
                InkWell(
                    onTap: () {
                      UserRepository.of(context).logOutUser(context);
                    },
                    child: Image.asset(
                      "assets/images/logout.png",
                      scale: 1.5,
                    )),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: List.generate(DrawerVM.drawerOptions.length, (index) {
                DrawerOption option = DrawerVM.drawerOptions[index];
                return Padding(
                  padding: const EdgeInsets.fromLTRB(8, 8, 20, 8),
                  child: InkWell(
                    onTap: () {
                      if (option.onPress != null) {
                        option.onPress!(context);
                      } else {
                        _key.currentState!.closeEndDrawer();

                        if(option.title=="HOME"){
                          MyNavigator.pushAndRemoveUntil(
                              context,
                              DraweredView(
                                title: option.label ?? option.title.toString(),
                                child: option.child ?? Container(),
                              ));
                        }
                        else {
                          MyNavigator.push(
                              context,
                              DraweredView(
                                title: option.label ?? option.title.toString(),
                                child: option.child ?? Container(),
                              ));
                        }

                      }
                    },
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            option.title.toString(),
                            textAlign: TextAlign.right,
                            style: theme.textTheme.titleMedium!
                                .copyWith(color: Colors.grey.shade800),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                              width: 22,
                              height: 0.5,
                              color: Colors.grey.shade800)
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ),
          )),
          Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Image.asset(
                      "assets/images/brand-wide@3x.png",
                      scale: 6,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Copyright 2019 Greenlam Industries Limited.\n"
                      "All rights reserved. App version : 2.0.1",
                      textAlign: TextAlign.center,
                      style: theme.textTheme.titleSmall!
                          .copyWith(color: Colors.grey.shade800, fontSize: 8),
                    )
                  ],
                ),
              ))
        ],
      ),
    );

    return ColoredSafeArea(
      child: WillPopScope(
        onWillPop: () async {
          if (widget.title == "HOME") {
            showExitDialogue(context);
          }

          return Future.value(true);
        },
        child: Scaffold(
          key: _key,
          endDrawer: endDrawer,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppBar(
                automaticallyImplyLeading: false,
                leading: InkWell(
                  onTap: (){
                    MyNavigator.pushAndRemoveUntil(context, Routes.home);
                  },
                  child: Image.asset(
                    "assets/images/logo@2x.png",
                    scale: 4,
                  ),
                ),
                title: Text(widget.title),
                actions: [
                  ((ModalRoute.of(context)?.canPop??false) && Platform.isIOS)? InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        "assets/images/left_arrow.png",
                        scale: 4,
                      )):Container(),
                  InkWell(
                      onTap: () {
                        _key.currentState!.openEndDrawer();
                      },
                      child: Image.asset(
                        "assets/images/hamburger.png",
                        scale: 3,
                      )),
                ],
              ),
              Expanded(child: widget.child)
            ],
          ),
        ),
      ),
    );
  }
}
