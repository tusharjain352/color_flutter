



import 'dart:io';

import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/widget/image_picker/src/widgets/picker.dart';
import 'package:flutter/material.dart';

class ImageSelector extends StatelessWidget {

  final String filePath;
  final Function(Color color) onColorPicked;
  final Widget? child;
  const ImageSelector({Key? key, this.filePath="", required this.onColorPicked, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Widget fileWidget=Container(
      height: 350,
      decoration: BoxDecoration(
          color: AppColor.white,
          border: Border.all(
              color: Colors.black87
          ),
          image: filePath==""?const DecorationImage(
              image: AssetImage("assets/images/no-image.jpg")
          ):
          DecorationImage(
              image: FileImage(File(filePath)),
              fit: BoxFit.fill
          )
      ),
      child: child,
    );
    return filePath.isEmpty?
    fileWidget
        :ImageColorPicker(
      onChanged: (Color color){
        if(filePath!=""){
          onColorPicked(color);
        }
      },
      child: fileWidget,
    );


  }
}
