import 'package:colurama/app_manager/extension/color_extention.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/routes.dart';
import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:flutter/material.dart';

class DesignsWidget extends StatelessWidget {
  final List<Design> designs;
  const DesignsWidget({Key? key, required this.designs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    return Padding(
      padding: const EdgeInsets.fromLTRB(15,0,15,15),
      child: DynamicHeightGridView(
        itemCount: designs.length,
          crossAxisCount: 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        builder: (BuildContext context, index) {
          Design design=designs[index];
          return InkWell(
            onTap: (){
              MyNavigator.push(context, Routes.designDetails(design));
            },
            child: Column(
              children: [
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey
                      ),
                      image: DecorationImage(
                        image: NetworkImage(design.thumbnailPath??""),
                        fit: BoxFit.cover,
                      )
                  ),

                ),
                const SizedBox(height: 8),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: [
                    Expanded(
                      child: Text(design.title??"",
                        style: theme.textTheme.titleSmall!.copyWith(
                            fontWeight: FontWeight.normal
                        ),),
                    ),
                    Container(
                      height: 20,
                      width: 20,
                      decoration: BoxDecoration(
                          color: design.hexcode.toString().toColor(),
                          border: Border.all(
                              color: Colors.grey
                          )
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
