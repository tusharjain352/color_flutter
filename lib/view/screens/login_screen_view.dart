

import 'package:colurama/app_manager/components/colored_safe_area.dart';
import 'package:colurama/app_manager/helper/exit_alert.dart';
import 'package:colurama/app_manager/helper/my_url_luancher.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/view_model/login_view_model.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreenView extends StatefulWidget {
  const LoginScreenView({Key? key}) : super(key: key);

  @override
  State<LoginScreenView> createState() => _LoginScreenViewState();
}

class _LoginScreenViewState extends State<LoginScreenView> {



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }


  void get() async {

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      LoginVM.of(context).initiateLoginScreen();
    });
  }





  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    LoginVM vm=LoginVM.of(context);
    return ColoredSafeArea(
      child: WillPopScope(
        onWillPop: () async{
          showExitDialogue(context);
          return Future.value(true);
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(30,40,40,0),
                  child: Form(
                    key: vm.formKey,
                    autovalidateMode: AutovalidateMode.disabled,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 20,),
                       Image.asset("assets/images/logo@3x.png",
                       scale: 2,),
                        const SizedBox(height: 30,),
                        Text("Welcome to Greenlam Colourama",
                        style: theme.textTheme.titleMedium,),
                        const SizedBox(height: 30,),
                        Text("Hi, I am",
                        style: theme.textTheme.titleSmall!.copyWith(
                          color: AppColor.primaryColor
                        )),
                        TextFormField(
                          controller: vm.nameC,
                          decoration: const InputDecoration(
                            hintText: "NAME"
                          ),
                          validator: (String? val) {
                            if (val == null || val.trim().isEmpty) {
                              return "Required field";
                            } else {
                              return null;
                            }
                          },
                          onChanged: (val){
                            vm.checkToEnableButton();
                          },
                        ),
                        const SizedBox(height: 30,),
                        Text("and you can reach me at",
                            style: theme.textTheme.titleSmall!.copyWith(
                                color: AppColor.primaryColor
                            )),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          maxLength: 10,
                          controller: vm.numberC,
                          decoration: const InputDecoration(
                              hintText: "MOBILE",
                            counterText: "",
                          ),
                          validator: (String? val) {
                            if (val == null || val.trim().isEmpty) {
                              return "Required field";
                            } else  if (val.length<10) {
                              return "Please enter a valid 10 digit mobile number";
                            } else {
                              return null;
                            }
                          },
                          onChanged: (val){
                            vm.checkToEnableButton();
                          },
                        ),

                        const SizedBox(height: 40,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Selector<LoginVM,bool>(
                                  selector: (buildContext, vm) => vm.readPolicy,
                                  builder: (context,bool data, child) {
                                  return     InkWell(
                                    onTap: (){
                                      vm.readPolicy=!data;
                                    },
                                    child: data?
                                    Image.asset( "assets/images/checked.png",
                                      scale: 4,)
                                    :Image.asset("assets/images/chk_unchkd.png",
                                      scale: 5,),
                                  );
                                }
                              ),
                            ),
                             Expanded(
                               child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                      text: 'I have read accept the ',
                                      style: theme.textTheme.titleSmall!.copyWith(
                                          fontWeight: FontWeight.normal
                                      )),
                                    TextSpan(
                                        text: 'Terms & Conditions',
                                        style: theme.textTheme.titleSmall!.copyWith(
                                          color: AppColor.secondaryColor,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () async{
                                            await MyUrlLauncher.launchInBrowser('https://www.greenlam.com/india/terms-conditions/');
                                          }
                                    ),
                                    TextSpan(
                                        text: ' and ',
                                        style: theme.textTheme.titleSmall!.copyWith(
                                            fontWeight: FontWeight.normal
                                        )),
                                    TextSpan(
                                        text: 'Privacy Policy.',
                                        style: theme.textTheme.titleSmall!.copyWith(
                                            color: AppColor.secondaryColor,
                                            fontWeight: FontWeight.normal
                                        ),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () async{
                                            await MyUrlLauncher.launchInBrowser('https://www.greenlam.com/india/privacy-policy');
                                          }
                                    ),
                                  ],
                                ),
                            ),
                             )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SizedBox(
                      width: double.infinity,
                      child:  Selector<LoginVM,bool>(
                          selector: (buildContext, vm) => vm.enableButton,
                          builder: (context,bool data, child) {
                          return TextButton(
                              style: TextButton.styleFrom(
                                backgroundColor: !data? Colors.grey.shade400:null,
                                side:  data? null:BorderSide(
                                  color: !data? Colors.grey.shade400:AppColor.primaryColor,
                                ),
                              ),

                              onPressed: !data? null:(){
                                vm.onPressNext(context);
                              }, child:  Text("NEXT",
                          style: TextStyle(
                            color: !data? Colors.black:null,
                          ),));
                        }
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
