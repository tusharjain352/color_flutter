
import 'package:colurama/app_manager/extension/color_extention.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/authentication/user_repository.dart';
import 'package:colurama/model/user.dart';
import 'package:colurama/routes.dart';
import 'package:colurama/view/widget/button_border.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreenView extends StatefulWidget {
  const HomeScreenView({Key? key}) : super(key: key);

  @override
  State<HomeScreenView> createState() => _HomeScreenViewState();
}

class _HomeScreenViewState extends State<HomeScreenView> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    Widget floatingButton = ButtonBorder(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: InkWell(
            onTap: () {
              MyNavigator.push(context, Routes.imageShade);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/camera.png",
                  height: 18,
                  //////////////////
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  "IMAGE",
                  style: theme.textTheme.labelSmall,
                )
              ],
            ),
          )),
          const VerticalDivider(
            color: AppColor.black,
            thickness: 2,
          ),
          Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                      30,
                      0,
                      30,
                      0,
                    ),
                    child: Image.asset("assets/images/colourama.png"),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Select an Option",
                    style: theme.textTheme.labelSmall,
                  )
                ],
              )),
          const VerticalDivider(
            color: AppColor.black,
            thickness: 2,
          ),
          Expanded(
              child: InkWell(
            onTap: () {
              MyNavigator.push(context, Routes.selectShade);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                  child: Padding(
                    padding: const EdgeInsets.all(1.0),
                    child: Image.asset(
                      "assets/images/picker.png",
                      height: 30,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  "PICKER",
                  style: theme.textTheme.labelSmall,
                )
              ],
            ),
          )),
        ],
      ),
    );

    return Stack(
      children: [
        SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  15,
                  8,
                  8,
                  8,
                ),
                child: Selector<UserRepository, User>(
                    shouldRebuild: (prev, nex) => true,
                    selector: (buildContext, vm) => vm.getUser,
                    builder: (context, User data, child) {
                      return Text(
                        "Hi ${data.name.toString()}",
                        style: theme.textTheme.titleSmall!.copyWith(
                            fontWeight: FontWeight.normal,
                            color: darken(Colors.green, 0.2)),
                      );
                    }),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(
                  15,
                  8,
                  8,
                  8,
                ),
                child: Text(
                  "Welcome to the world of interior surfaces!",
                  style: theme.textTheme.titleLarge!.copyWith(),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 800,
                child: Image.asset(
                  "assets/images/banner@2x.png",
                  fit: BoxFit.fitHeight,
                ),
              )
            ],
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: floatingButton,
            ),
          ),
        )
      ],
    );
  }
}
