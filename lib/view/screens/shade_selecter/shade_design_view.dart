



import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/api/manage_response.dart';
import 'package:colurama/app_manager/extension/color_extention.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/view/widget/button_border.dart';
import 'package:colurama/view/widget/designs_widget.dart';
import 'package:colurama/view_model/select_shade_vm.dart';
import 'package:colurama/view_model/shade_designs_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShadeDesignView extends StatefulWidget {

  final String shadeHexCode;


  const ShadeDesignView({Key? key, required this.shadeHexCode, }) : super(key: key);

  @override
  State<ShadeDesignView> createState() => _ShadeDesignViewState();
}

class _ShadeDesignViewState extends State<ShadeDesignView> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }

  late String shadeId;

  void get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      ShadeDesignsVM.of(context).fetchDesigns(context,
      shadeHex: widget.shadeHexCode);
    });
  }

  @override
  Widget build(BuildContext context) {

    ShadeDesignsVM vm=ShadeDesignsVM.of(context);
    final theme=Theme.of(context);

    Widget floatingButton = ButtonBorder(
      child: Selector<SelectShadeVM, String?>(
          shouldRebuild: (prev, nex) => true,
          selector: (buildContext, vm) => vm.selectedShade,
          builder: (context, String? data, child) {
            String? shade = data;
            return Row(
              children: [
                Expanded(
                    child: InkWell(
                      onTap: () {
                        if (shade != null) {
                          MyNavigator.pop(context);
                        }
                      },
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8),
                            child: Image.asset(
                              "assets/images/left_arrow_blue.png",
                              scale: 1.5,
                            ),
                          ),
                          Text(
                            "CHANGE SHADE".toUpperCase(),
                            style: theme.textTheme.bodySmall!.copyWith(
                                color: shade != null
                                    ? AppColor.primaryColor
                                    : AppColor.greyDark),
                          ),
                        ],
                      ),
                    )),
                const VerticalDivider(
                  color: AppColor.black,
                  thickness: 2,
                ),
                Expanded(
                    child: Wrap(
                      alignment: WrapAlignment.end,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                                color: shade==null? null:(shade).toColor(),
                                border: Border.all(color: AppColor.black)),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Selected Shade".toUpperCase(),
                          style: theme.textTheme.bodySmall!
                              .copyWith(color: AppColor.primaryColor),
                        ),
                      ],
                    )),
              ],
            );
          }),
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
          Padding(
        padding: const EdgeInsets.all(15,),
        child: Text("Your Selected Shade Designs",
        style: TextStyle(
          color: Colors.grey.shade700,
          fontWeight: FontWeight.w400
        ),),),
        Expanded(
          child: Selector<ShadeDesignsVM,ApiResponse<List<Design>>>(
              selector: (buildContext, vm) => vm.designsResponse,
              builder: (context,ApiResponse<List<Design>> data, child) {

                return  ManageResponse(
                    response: data,
                    onPressRetry: (){
                      vm.fetchDesigns(context, shadeHex:widget.shadeHexCode);
                    },  child: DesignsWidget(designs:(vm.designsResponse.data??[])),);
            }
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(15,0,15,15),
          child: floatingButton,
        )
      ],
    );
  }
}
