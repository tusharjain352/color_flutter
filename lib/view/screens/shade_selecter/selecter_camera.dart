import 'package:camera/camera.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/helper/progress_dialogue.dart';
import 'package:colurama/view_model/select_shade_vm.dart';
import 'package:flutter/material.dart';


class SelectorCamera extends StatefulWidget {
  final Function(String filePath) onPhotoClicked;
  
  const SelectorCamera({Key? key, required this.onPhotoClicked}) : super(key: key);

  @override
  State<SelectorCamera> createState() => _SelectorCameraState();
}

class _SelectorCameraState extends State<SelectorCamera> {
  late CameraController controller;

  @override
  void initState() {
    super.initState();
    get();
  }

  get() async {
    _initCamera(SelectShadeVM.of(context).getCameras.first);
  }


  Future<void> _initCamera(CameraDescription description) async{
    controller = CameraController(description, ResolutionPreset.max, enableAudio: true);

    try{
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      }).catchError((Object e) {
        if (e is CameraException) {
          switch (e.code) {
            case 'CameraAccessDenied':
              Alert.show("CameraAccessDenied");
              break;
            default:
              Alert.show("CameraAccessDenied");
              break;
          }
        }
      });
    }
    catch(e){
      Alert.show(e);
    }
  }


  void _toggleCameraLens() {
    final lensDirection = controller.description.lensDirection;
    CameraDescription? newDescription;
    if (lensDirection == CameraLensDirection.front) {
      newDescription = SelectShadeVM.of(context).getCameras.firstWhere((description) => description
          .lensDirection == CameraLensDirection.back);
    }
    else {
      newDescription = SelectShadeVM.of(context).getCameras.firstWhere((description) => description
          .lensDirection == CameraLensDirection.front);
    }

    _initCamera(newDescription);
  }



  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return Container();
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Stack(
        children: [
          SizedBox(
            width: double.infinity,
            child: CameraPreview(controller),
          ),
          Positioned(
            right: 0,
            left: 0,
            bottom: 0,
            child: InkWell(
            // Provide an onPressed callback.
            onTap: () async {
              // Take the Picture in a try / catch block. If anything goes wrong,
              // catch the error.
              try {
                PD.show(context, message: "");
                final image = await controller.takePicture();
                PD.hide();
                widget.onPhotoClicked(image.path);
              } catch (e) {
                PD.hide();
                Alert.show(e);
              }
            },
            child: Image.asset("assets/images/camera_gray.png",
            height: 60,),
          ),),
          Positioned(
            right: 10,
            top: 10,
            child: InkWell(
              onTap: () async {
                _toggleCameraLens();
              },
              child: Image.asset("assets/images/camera_front_gray.png",
                height: 30,),
            ),)
        ],
      ),
    );
  }
}