
import 'package:colurama/app_manager/extension/color_extention.dart';
import 'package:colurama/app_manager/helper/custom_image_picker.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/view/screens/shade_selecter/selecter_camera.dart';
import 'package:colurama/view/widget/button_border.dart';
import 'package:colurama/view/widget/image_selector.dart';
import 'package:colurama/view_model/select_shade_vm.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class ImageShadeView extends StatefulWidget {
  const ImageShadeView({Key? key}) : super(key: key);

  @override
  State<ImageShadeView> createState() => _ImageShadeViewState();
}

class _ImageShadeViewState extends State<ImageShadeView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }

  void get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      SelectShadeVM.of(context).initiateIS(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final SelectShadeVM vm = SelectShadeVM.of(context);

    Widget floatingButton = ButtonBorder(
      child: Selector<SelectShadeVM, String?>(
          shouldRebuild: (prev, nex) => true,
          selector: (buildContext, vm) => vm.selectedShade,
          builder: (context, String? data, child) {
            String? color = data;
            return Row(
              children: [
                Expanded(
                    child: InkWell(
                  onTap: () async {
                    vm.cameraSelected=!vm.cameraSelected;
                    // vm.selectedFile =
                    //     ((await CustomImagePicker.pickImageFromCamera())
                    //             as CroppedFile)
                    //         .path;
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/camera.png",
                        height: 16,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        "CAMERA",
                        style: theme.textTheme.labelSmall,
                      )
                    ],
                  ),
                )),
                const VerticalDivider(
                  color: AppColor.black,
                  thickness: 1,
                ),
                Expanded(
                    child: InkWell(
                  onTap: () async {
                    vm.selectedFile =
                        ((await CustomImagePicker.pickImageFromGallery())
                                as CroppedFile)
                            .path;
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/images/gallery_icon.png",
                        height: 18,
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                        "GALLERY",
                        style: theme.textTheme.labelSmall,
                      )
                    ],
                  ),
                )),
                const VerticalDivider(
                  color: AppColor.black,
                  thickness: 1,
                ),
                Expanded(
                    flex: 2,
                    child: InkWell(
                      onTap: () {
                        if (color != null) {
                       vm.onPressViewDesign(context, color);
                        }
                      },
                      child: Wrap(
                        alignment: WrapAlignment.end,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 7, 8, 8),
                            // padding: const EdgeInsets.all(8.0),
                            child: Container(
                              height: 20,
                              width: 20,
                              decoration: BoxDecoration(
                                  color: color==null? null:(color.toString().toColor()),
                                  border: Border.all(color: AppColor.black)),
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text(
                            "USE SHADE".toUpperCase(),
                            style: theme.textTheme.bodySmall!.copyWith(
                                color: AppColor.primaryColor
                                // color: color!=null? AppColor.primaryColor:
                                // AppColor.greyDark
                                ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Image.asset(
                              "assets/images/right_arrow_blue.png",
                              scale: 1.1,
                            ),
                          ),
                        ],
                      ),
                    )),
              ],
            );
          }),
    );
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: SingleChildScrollView(
            child: Selector<SelectShadeVM, Tuple2<String,bool>>(
                shouldRebuild: (prev, nex) => true,
                selector: (buildContext, vm) => Tuple2(vm.selectedFile, vm.cameraSelected),
                builder: (context, Tuple2<String,bool> data, child) {
                  bool imageSelected = data.item1 != "";
                  bool cameraSelected=data.item2;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        imageSelected
                            ? "Pick a colour from your selected image"
                            : "Pick an image",
                        style: theme.textTheme.titleMedium!.copyWith(
                          color: Colors.grey.shade700,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        imageSelected
                            ? "Great! Now choose a colour from your selected image."
                            : "Choose an image either from your camera or using gallery.",
                        style: theme.textTheme.titleMedium!.copyWith(
                            color: AppColor.greyDark,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ImageSelector(
                        filePath: data.item1,
                        onColorPicked: (Color color) {
                          vm.selectedShade = color.toHex();
                        },
                        child: cameraSelected?  SelectorCamera(
                          onPhotoClicked: (String val){
                            vm.cameraSelected=false;
                            vm.selectedFile=val;

                          },
                        ):null,
                      )
                    ],
                  );
                }),
          )),
          floatingButton,
        ],
      ),
    );
  }
}
