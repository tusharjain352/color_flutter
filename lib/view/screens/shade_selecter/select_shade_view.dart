import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/api/manage_response.dart';
import 'package:colurama/app_manager/extension/color_extention.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/color_data.dart';
import 'package:colurama/model/shade.dart';
import 'package:colurama/view/widget/button_border.dart';
import 'package:colurama/view_model/select_shade_vm.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class SelectShadeView extends StatefulWidget {
  const SelectShadeView({Key? key}) : super(key: key);

  @override
  State<SelectShadeView> createState() => _SelectShadeViewState();
}

class _SelectShadeViewState extends State<SelectShadeView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }

  void get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      SelectShadeVM.of(context).initiateSS(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    SelectShadeVM vm = SelectShadeVM.of(context);
    final theme = Theme.of(context);

    Widget floatingButton = ButtonBorder(
      child: Selector<SelectShadeVM, String?>(
          shouldRebuild: (prev, nex) => true,
          selector: (buildContext, vm) => vm.selectedShade,
          builder: (context, String? data, child) {
            String? shade = data;
            return Row(
              children: [
                Expanded(
                    child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                            color: shade==null? null:(shade).toColor(),
                            border: Border.all(color: AppColor.black)),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        "Selected Shade".toUpperCase(),
                        style: theme.textTheme.bodySmall!
                            .copyWith(color: AppColor.primaryColor),
                      ),
                    ),
                  ],
                )),
                const VerticalDivider(
                  color: AppColor.black,
                  thickness: 2,
                ),
                Expanded(
                    child: InkWell(
                  onTap: () {
                    if (shade != null) {
                      vm.onPressViewDesign(context, shade);
                    }
                  },
                  child: Wrap(
                    alignment: WrapAlignment.end,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text(
                        "VIEW DESIGN".toUpperCase(),
                        style: theme.textTheme.bodySmall!.copyWith(
                            color: shade != null
                                ? AppColor.primaryColor
                                : AppColor.greyDark),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Image.asset(
                          "assets/images/right_arrow_grey.png",
                          scale: 1.5,
                        ),
                      ),
                    ],
                  ),
                )),
              ],
            );
          }),
    );

    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Choose a colour",
            style:
                theme.textTheme.titleMedium!.copyWith(color: AppColor.greyDark),
          ),
          const SizedBox(
            height: 15,
          ),
          Selector<SelectShadeVM, Tuple2<ApiResponse<List<ColorData>>, ColorData>>(
              shouldRebuild: (prev, nex) => true,
              selector: (buildContext, vm) =>
                  Tuple2(vm.colorsResponse, vm.selectedColorData),
              builder: (context,
                  Tuple2<ApiResponse<List<ColorData>>, ColorData> data, child) {
                ApiResponse<List<ColorData>> response = data.item1;
                List<ColorData> colors = response.data ?? [];

                ColorData selectedColor = data.item2;

                return ManageResponse(
                  response: response,
                  axis: Axis.horizontal,
                  showImage: false,
                  onPressRetry: () {
                    vm.fetchColors(context);
                  },
                  child: SizedBox(
                    height: 50,
                    child: ListView.builder(
                        itemCount: colors.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          ColorData color = colors[index];

                          bool selected = selectedColor.id == color.id;

                          return InkWell(
                            customBorder: const CircleBorder(),
                            onTap: () {
                              vm.selectedColorData = color;
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  color: selected
                                      ? (color.hexcode ?? "").toColor()
                                      : AppColor.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: AppColor.black, width: 1.2)),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      color: (color.hexcode ?? "").toColor(),
                                      shape: BoxShape.circle),
                                  child: selected
                                      ? Image.asset(
                                          "assets/images/down_arrow.png")
                                      : Container(),
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                );
              }),
          const SizedBox(
            height: 30,
          ),
          Text(
            "Select a shade",
            style:
                theme.textTheme.titleMedium!.copyWith(color: AppColor.greyDark),
          ),
          const SizedBox(
            height: 15,
          ),
          Expanded(
            child: Selector<SelectShadeVM,
                    Tuple4<ApiResponse<List<Shade>>, List<Shade>, String?,ColorData>>(
                shouldRebuild: (prev, nex) => true,
                selector: (buildContext, vm) =>
                    Tuple4(vm.shadeResponse, vm.shades, vm.selectedShade,vm.selectedColorData),
                builder: (context,
                    Tuple4<ApiResponse<List<Shade>>, List<Shade>, String?,ColorData> data,
                    child) {
                  ApiResponse<List<Shade>> response = data.item1;
                  List<Shade> shades = data.item2;

                  String? selectedShades = data.item3;
                  ColorData selectedColorData=data.item4;

                  return selectedColorData.hexcode!=null?
                  Padding(
                    padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                    child: ListView.builder(
                        itemCount: 20,
                        reverse: true,
                        itemBuilder: (context, index) {

                          Color color=lighten(darken(selectedColorData.hexcode.toString().toColor(),0.2),(index*0.02));

                          bool selected = selectedShades == color.toHex();



                          return InkWell(
                            customBorder: const CircleBorder(),
                            onTap: () {
                              vm.selectedShade = color.toHex();
                            },
                            child: Container(
                              height: 50,
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: AppColor.greyDark, width: 1.2),
                                color: color,
                              ),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: selected
                                      ? Image.asset(
                                      "assets/images/checked_white.png")
                                      : Image.asset(
                                      "assets/images/unchecked_white.png"),
                                ),
                              ),
                            ),
                          );
                        }),
                  )
                  :ManageResponse(
                    response: response,
                    axis: Axis.horizontal,
                    showImage: false,
                    onPressRetry: () {
                      vm.fetchShades(context);
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child: ListView.builder(
                          itemCount: shades.length,
                          itemBuilder: (context, index) {
                            Shade shade = shades[index];
                            bool selected = selectedShades == shade.hexcode;

                            return InkWell(
                              customBorder: const CircleBorder(),
                              onTap: () {
                                vm.selectedShade = shade.hexcode;
                              },
                              child: Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: AppColor.greyDark, width: 1.2),
                                  color: (shade.hexcode ?? "").toColor(),
                                ),
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: selected
                                        ? Image.asset(
                                            "assets/images/checked_white.png")
                                        : Image.asset(
                                            "assets/images/unchecked_white.png"),
                                  ),
                                ),
                              ),
                            );
                          }),
                    ),
                  );
                }),
          ),
          const SizedBox(
            height: 30,
          ),
          floatingButton
        ],
      ),
    );
  }
}
