
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/components/drop_down.dart';
import 'package:colurama/app_manager/extension/is_valid_email.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/model/state_data.dart';
import 'package:colurama/view/screens/drawer_screens/design_view/design_view.dart';
import 'package:colurama/view/widget/drawered_view.dart';
import 'package:colurama/view_model/country_view_model.dart';
import 'package:colurama/view_model/enquire_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class EnquireNowView extends StatefulWidget {
  final Design design;
  final List<String> selectedComplement;
  const EnquireNowView({Key? key, required this.design, required this.selectedComplement}) : super(key: key);

  @override
  State<EnquireNowView> createState() => _EnquireNowViewState();
}

class _EnquireNowViewState extends State<EnquireNowView> {


  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }

  get() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      EnquireViewModel.of(context).initiate(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    final EnquireViewModel vm=EnquireViewModel.of(context);

    bool isIndia=CountryVM.of(context).getSelectedCountry.id==1;


    Widget citySelection=
        isIndia? Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('State',
            style: theme.textTheme.titleSmall!
                .copyWith(color: AppColor.primaryColor)),
        Selector<CountryVM, Tuple2<ApiResponse<List<StateData>>,StateData?>>(
            shouldRebuild: (prev, nex) => true,
            selector: (buildContext, cvm) => Tuple2(cvm.stateResponse, cvm.selectState),
            builder: (context, Tuple2<ApiResponse<List<StateData>>,StateData?> data, child) {
              List<StateData> states=data.item1.data??[];
              bool loading=data.item1.status==Status.loading;

              StateData? selectedState=data.item2;

              return loading?  Padding(
                padding: const EdgeInsets.fromLTRB(0,8,0,8),
                child: Text("Loading States",
                  style: theme.textTheme.titleMedium!.copyWith(
                      color: Colors.grey.shade700
                  ),),
              ):SizedBox(
                width: double.infinity,
                child: MyDropDown<String>(
                  hint: "SELECT STATE",
                  value: selectedState?.name.toString(),
                  isExpanded: true,
                  items: List.generate(states.length, (index) =>
                      DropdownMenuItem(
                          value: states[index].name,
                          child: Text(states[index].name??""))
                  ),
                  onChanged: (String? state){
                    if(state!=null && states.isNotEmpty){
                      CountryVM.of(context).selectState=states.firstWhere((element) => element.name==state);
                      CountryVM.of(context).selectCity=null;
                    }
                  },
                ),
              );
            }
        ),
        const SizedBox(height: 20),
        Text('City',
            style: theme.textTheme.titleSmall!
                .copyWith(color: AppColor.primaryColor)),
        Selector<CountryVM, Tuple3<ApiResponse<List<StateData>>,StateData?,Cities?>>(
            shouldRebuild: (prev, nex) => true,
            selector: (buildContext, cvm) => Tuple3(cvm.stateResponse, cvm.selectState,cvm.selectCity),
            builder: (context, Tuple3<ApiResponse<List<StateData>>,StateData?,Cities?> data, child) {
              List<StateData> states=data.item1.data??[];

              StateData? selectedState=data.item2;
              Cities? selectedCity=data.item3;
              List<Cities> cities=[];
              if(selectedState!=null && states.isNotEmpty){
                cities=states.firstWhere((element) => element.id.toString()==selectedState.id.toString()).cities??[];
              }



              return SizedBox(
                width: double.infinity,
                child: MyDropDown<String>(
                  hint: "SELECT CITY",
                  value: selectedCity?.name.toString(),
                  enableFeedback: selectedState!=null,
                  isExpanded: true,
                  items: selectedState==null? []:List.generate(cities.length, (index) =>
                      DropdownMenuItem(
                          value: cities[index].name.toString(),
                          child: Text(cities[index].name??""))
                  ),
                  onChanged: (String? city){
                    if(city!=null && cities.isNotEmpty){
                      CountryVM.of(context).selectCity=cities.firstWhere((element) => element.name==city);
                    }
                  },
                ),
              );
            }
        ),
      ],
    )
    :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('City',
                style: theme.textTheme.titleSmall!
                    .copyWith(color: AppColor.primaryColor)),
            TextFormField(
              controller: vm.cityC,
              decoration: const InputDecoration(
                  hintText: "CITY"
              ),
              validator: (String? val) {
                if (val == null || val.trim().isEmpty) {
                  return "Required field";
                } else {
                  return null;
                }
              },
            ),
            const SizedBox(height: 20),
            Text('PO. Box',
                style: theme.textTheme.titleSmall!
                    .copyWith(color: AppColor.primaryColor)),
            TextFormField(
              controller: vm.poBoxC,
              decoration: const InputDecoration(
                  hintText: "PO. Box"
              ),
              validator: (String? val) {
                if (val == null || val.trim().isEmpty) {
                  return "Required field";
                } else {
                  return null;
                }
              },
            ),
          ],
        );

    return DraweredView(
        title: "GET YOUR LAMINATE",
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              children: [
                DesignView(
                  design: widget.design,
                  selectedComplement: widget.selectedComplement,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20,0,20,20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Excellent Choice! Please share your details for us to assist you with your requirement.",
                        style: theme.textTheme.titleMedium!.copyWith(
                            color: Colors.grey.shade700
                        ),),
                      const SizedBox(height: 20),
                      Text('Your name',
                          style: theme.textTheme.titleSmall!
                              .copyWith(color: AppColor.primaryColor)),
                      TextFormField(
                        controller: vm.nameC,
                        decoration: const InputDecoration(
                            hintText: "NAME"
                        ),
                        validator: (String? val) {
                          if (val == null || val.trim().isEmpty) {
                            return "Required field";
                          } else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(height: 30),
                      Text('Your mobile',
                          style: theme.textTheme.titleSmall!
                              .copyWith(color: AppColor.primaryColor)),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        maxLength: 10,
                        controller: vm.numberC,
                        decoration: const InputDecoration(
                          hintText: "MOBILE",
                          counterText: "",
                        ),
                        validator: (String? val) {
                          if (val == null || val.trim().isEmpty) {
                            return "Required field";
                          } else  if (val.length<10) {
                            return "Please enter a valid 10 digit mobile number";
                          } else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(height: 30),
                      Text('Your email',
                          style: theme.textTheme.titleSmall!
                              .copyWith(color: AppColor.primaryColor)),
                      TextFormField(
                        controller: vm.emailC,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                            hintText: "EMAIL"
                        ),
                        validator: (String? val) {
                          if (val == null || val.trim().isEmpty) {
                            return "Required field";
                          }
                          else  if (!val.isValidEmail()) {
                            return "Please enter a valid email address";
                          }
                          else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(height: 30),
                      Text('Pincode',
                          style: theme.textTheme.titleSmall!
                              .copyWith(color: AppColor.primaryColor)),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        maxLength: 6,
                        controller: vm.pincodeC,
                        decoration: const InputDecoration(
                          hintText: "Pincode",
                          counterText: "",
                        ),
                        validator: (String? val) {
                          if (val == null || val.trim().isEmpty) {
                            return "Required field";
                          } else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(height: 20),
                      Text('Country',
                          style: theme.textTheme.titleSmall!
                              .copyWith(color: AppColor.primaryColor)),
                      TextFormField(
                        controller: vm.countryC,
                        enabled: false,
                        decoration: const InputDecoration(
                            hintText: "Country"
                        ),
                        validator: (String? val) {
                          if (val == null || val.trim().isEmpty) {
                            return "Required field";
                          } else {
                            return null;
                          }
                        },
                      ),
                      const SizedBox(height: 20),
                      citySelection,

                      const SizedBox(height: 20),
                      SizedBox(
                        width: double.infinity,
                        child: TextButton(
                          onPressed: () {
                            if(formKey.currentState!.validate()){
                              vm.onPressSubmit(context,
                              design: widget.design,
                              selectedComplement: widget.selectedComplement);
                            }
                            else {
                              Alert.show("Fields are not validated");
                            }
                          },
                          child: const Text('SUBMIT'),
                        ),
                      ),
                      const SizedBox(height: 100),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
