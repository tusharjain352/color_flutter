

import 'package:colurama/app_manager/components/colored_safe_area.dart';
import 'package:colurama/app_manager/constant/project_constant.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/inspiration.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

class GalleryPageView extends StatefulWidget {

  final List<Inspiration> inspirations;
  final int indexPressed;

  const GalleryPageView({super.key, this.indexPressed=0, required this.inspirations});

  @override
  State<GalleryPageView> createState() => _GalleryPageViewState();
}

class _GalleryPageViewState extends State<GalleryPageView> {


  int _currentIndex=0;


  late PageController _pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex=widget.indexPressed;
    _pageController = PageController(initialPage: _currentIndex);
  }


  @override
  Widget build(BuildContext context) {

    List<String> imageList = widget.inspirations.map((e) => e.imageLargePath.toString()).toList();
    final theme=Theme.of(context);
    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: AppColor.black,
        appBar: AppBar(
          backgroundColor: AppColor.black,
          automaticallyImplyLeading: false,
          title: Text("${_currentIndex+1}/${imageList.length}",
          style: theme.textTheme.titleMedium!.copyWith(
            color: AppColor.white
          ),),
          actions: [
           InkWell(
             onTap: (){
               MyNavigator.pop(context);
             },
             child: Image.asset("assets/images/cross_white.png",
             scale: 3,),
           ),
          ],
        ),
        // Implemented with a PageView, simpler than setting it up yourself
        // You can either specify images directly or by using a builder as in this tutorial
        body: Column(
          children: [
            Expanded(
              child: PhotoViewGallery.builder(
                pageController: _pageController,
                itemCount: imageList.length,
                builder: (context, index) {
                  return PhotoViewGalleryPageOptions(
                    imageProvider: NetworkImage(
                      imageList[index],
                    ),
                    // Contained = the smallest possible size to fit one dimension of the screen
                    // minScale: PhotoViewComputedScale.contained * 0.8,
                    // // Covered = the smallest possible size to fit the whole screen
                    // maxScale: PhotoViewComputedScale.covered * 2,
                  );
                },
                // Set the background color to the "classic white"
                backgroundDecoration: const BoxDecoration(
                  color: AppColor.black,
                ),
                loadingBuilder: (context, ice){
                  return ProjectConstant.placeHolder;
                },

                onPageChanged: (index){
                  _currentIndex=index;
                  setState(() {

                  });
                },
              ),
            ),

            Container(
              color: AppColor.white,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 30,20,30),
                  child: Text(widget.inspirations[_currentIndex].title??"",
                  style: theme.textTheme.titleMedium!.copyWith(
                    color: AppColor.greyDark
                  ),),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
