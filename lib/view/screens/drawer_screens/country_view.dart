
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/api/manage_response.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/country.dart';
import 'package:colurama/view_model/country_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class CountryView extends StatefulWidget {
  const CountryView({Key? key}) : super(key: key);

  @override
  State<CountryView> createState() => _CountryViewState();
}

class _CountryViewState extends State<CountryView> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }


  void get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      CountryVM.of(context).fetchCountries(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    CountryVM vm=CountryVM.of(context);
    final theme=Theme.of(context);
    return            Selector<CountryVM,Tuple2<ApiResponse<List<Country>>,Country>>(
        shouldRebuild: (prev, nex) => true,
        selector: (buildContext, vm) => Tuple2(vm.countryResponse, vm.getSelectedCountry),
        builder: (context,Tuple2<ApiResponse<List<Country>>,Country> data, child) {
          ApiResponse<List<Country>> response=data.item1;
          List<Country> countries=response.data??[];

          Country selectedCountry=data.item2;
          
          return ManageResponse(
            response: response,
            onPressRetry: (){
              vm.fetchCountries(context);
            },
            child: ListView.builder(
                itemCount: countries.length,
                padding: const EdgeInsets.all(22.0),
                itemBuilder: (context,index){
                  Country country=countries[index];
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: InkWell(
                      onTap: (){
                        vm.updateSCAndMoveToHome(context,country);
                      },
                      child: Container(
                        decoration:  BoxDecoration(
                          color: AppColor.white,
                          border: Border.all(
                            color: AppColor.black,
                            width: 1.2
                          ),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            children: [
                              Container(
                                height: 30,
                                width: 50,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage(country.thumbnailPath??""),
                                      fit: BoxFit.cover,
                                    )
                                ),

                              ),
                              const SizedBox(width: 20,),
                              Expanded(child: Text(country.name??"",
                              style: theme.textTheme.titleMedium,)),
                              const SizedBox(width: 20,),
                              (country.id==selectedCountry.id)?
                              Image.asset("assets/images/check.png",scale: 25,):Container()
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          );
        }
    );
  }
}
