
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/api/manage_response.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/model/inspiration.dart';
import 'package:colurama/view/screens/gallery_page_view.dart';
import 'package:colurama/view_model/inspiration_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InspirationView extends StatefulWidget {
  const InspirationView({Key? key}) : super(key: key);

  @override
  State<InspirationView> createState() => _InspirationViewState();
}

class _InspirationViewState extends State<InspirationView> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }


  void get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      InspirationVM.of(context).fetchInspirations(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    InspirationVM vm=InspirationVM.of(context);
    return            Selector<InspirationVM,ApiResponse<List<Inspiration>>>(
        shouldRebuild: (prev, nex) => true,
        selector: (buildContext, vm) => vm.inspirationResponse,
        builder: (context,ApiResponse<List<Inspiration>> data, child) {
          ApiResponse<List<Inspiration>> response=data;
          List<Inspiration> inspirations=response.data??[];


          return ManageResponse(
            response: response,
            onPressRetry: (){
              vm.fetchInspirations(context);
            },
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 150,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5,
              ),
                itemCount: inspirations.length,
                padding: const EdgeInsets.all(15.0),
                itemBuilder: (context,index){
                  Inspiration inspiration=inspirations[index];
                  return   InkWell(
                    onTap: (){
                      MyNavigator.push(context, GalleryPageView(
                        inspirations: inspirations,
                        indexPressed:index
                      ));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(inspiration.thumbnailPath??""),
                            fit: BoxFit.cover,
                          )
                      ),
                    ),
                  );
                }),
          );
        }
    );
  }
}
