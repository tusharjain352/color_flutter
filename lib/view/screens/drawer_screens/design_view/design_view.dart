


import 'package:colurama/app_manager/extension/color_extention.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/view/screens/drawer_screens/design_view/widget/complementary_design_view.dart';

import 'package:colurama/view/screens/drawer_screens/design_view/widget/main_design.dart';
import 'package:colurama/view/screens/enquire_now_view.dart';
import 'package:colurama/view/widget/button_border.dart';
import 'package:colurama/view_model/select_design_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DesignView extends StatefulWidget {
  final Design design;
  final List<String>? selectedComplement;
  const DesignView({Key? key, required this.design, this.selectedComplement}) : super(key: key);

  @override
  State<DesignView> createState() => _DesignViewState();
}

class _DesignViewState extends State<DesignView> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }
  get() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      SelectDesignViewModel.of(context).initiate();
    });
  }

  @override
  Widget build(BuildContext context) {

    bool insideEnquire=widget.selectedComplement!=null;

    final theme=Theme.of(context);

    ComplementaryDesign1 compDes1=widget.design.complementaryDesign1??ComplementaryDesign1();
    ComplementaryDesign1 compDes2=widget.design.complementaryDesign2??ComplementaryDesign1();


    bool showComp1=widget.selectedComplement==null || (widget.selectedComplement??[]).contains(compDes1.id.toString());
    bool showComp2=widget.selectedComplement==null || (widget.selectedComplement??[]).contains(compDes2.id.toString());

    Widget floatingButton = ButtonBorder(
      child: Row(
        children: [
          Expanded(
              child:     Padding(
                padding: const EdgeInsets.fromLTRB(10,15,0,15,),
                child: Selector<SelectDesignViewModel,List<String>>(
                    shouldRebuild: (prev, nex) => true,
                    selector: (buildContext, vm) => vm.selectedComplementary,
                    builder: (context,List<String> data, child) {
                    return data.isEmpty? Text(
                      "NO COMPLEMENTARY ADDED".toUpperCase(),
                      style: theme.textTheme.bodySmall!.copyWith(
                          color: AppColor.greyDark,
                        fontSize: 10
                      ),
                    ):Wrap(
                      children: [
                        data.contains(compDes1.id.toString())?Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                              color: compDes1.hexcode.toString().toColor(),
                              border: Border.all(
                                  color: Colors.grey
                              )
                          ),
                        ):Container(),
                        data.contains(compDes2.id.toString())?Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Container(
                            height: 20,
                            width: 20,
                            decoration: BoxDecoration(
                                color: compDes2.hexcode.toString().toColor(),
                                border: Border.all(
                                    color: Colors.grey
                                )
                            ),
                          ),
                        ):Container(),
                      ],
                    );
                  }
                ),
              ),),
          const VerticalDivider(
            color: AppColor.black,
            thickness: 1,
          ),
          Expanded(
              child: InkWell(
                onTap: () {
                 MyNavigator.push(context, EnquireNowView(design: widget.design,
                     selectedComplement: SelectDesignViewModel.of(context).selectedComplementary));
                },
                child: Wrap(
                  alignment: WrapAlignment.end,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [

                    Text(
                      "ENQUIRE NOW".toUpperCase(),
                      style: theme.textTheme.bodySmall!.copyWith(
                          color: AppColor.primaryColor
                        // color: color!=null? AppColor.primaryColor:
                        // AppColor.greyDark
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8,right: 8),
                      child: Image.asset(
                        "assets/images/cart.png",
                        scale: 4,
                      ),
                    ),
                  ],
                ),
              )),
        ],
      )
    );
    return Stack(
      children: [
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MainDesign(design: widget.design,
                  hideButton: insideEnquire,
                ),

                const SizedBox(height: 30),
                // Complementary
                Text("Complementary Laminates",
                style: theme.textTheme.titleMedium!.copyWith(
                  color: Colors.grey.shade700
                ),),
                (!showComp1 && !showComp2)? Padding(
                  padding: const EdgeInsets.only(top: 10,bottom: 10),
                  child: Text("No complementary selected.",
                    style: theme.textTheme.bodySmall!.copyWith(
                        color: AppColor.greyDark,
                        fontSize: 12
                    ),),
                ):Container(),
                compDes1.id==null? Container():(showComp1?Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: ComplementaryDesignView(design: compDes1,
                    hideButton: insideEnquire,),
                ):Container()),
                compDes2.id==null? Container():(showComp2?Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: ComplementaryDesignView(design: compDes2,
                    hideButton: insideEnquire,),
                ):Container()),
                insideEnquire? Container():const SizedBox(height: 100),
              ],
            ),
          ),
        ),
        Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: insideEnquire? Container():Padding(
              padding: const EdgeInsets.all(20.0),
              child: floatingButton,
            ))
      ],
    );
  }
}
