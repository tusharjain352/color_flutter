
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/view_model/select_design_view_model.dart';
import 'package:colurama/view_model/wishlist_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ComplementaryDesignView extends StatelessWidget {
  final ComplementaryDesign1 design;
  final bool hideButton;
  const ComplementaryDesignView({Key? key, required this.design, this.hideButton=false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 150,
          decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.grey
              ),
              image: DecorationImage(
                image: NetworkImage(design.thumbnailPath??""),
                fit: BoxFit.cover,
              )
          ),

        ),
        const SizedBox(height: 15),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(design.title??"",
                    style: theme.textTheme.titleSmall!.copyWith(
                        fontWeight: FontWeight.w500
                    ),),
                  hideButton?Text("Edgeband: ${design.edgeband?.name??""}",
                    style: theme.textTheme.titleSmall!.copyWith(
                        fontWeight: FontWeight.normal,
                        color: Colors.grey.shade700,
                        fontSize: 12
                    ),):Container(),
                ],
              ),
            ),
            const SizedBox(width: 20,),
            hideButton? Container(): InkWell(
                onTap: (){
                  WishListViewModel.of(context).addOrRemoveDesign(Design.fromJson(design.toJson()));
                },
                child: Selector<WishListViewModel, List<String>>(
                    shouldRebuild: (prev, nex) => true,
                    selector: (buildContext, vm) => vm.ids,
                    builder: (context, List<String> data, child) {
                      return Image.asset(
                        (data.contains(design.id.toString())?
                        "assets/images/heart_selected.png"
                            :"assets/images/heart_unselected.png"),
                        scale: 3,);
                    }
                )),
            hideButton? Container():const SizedBox(width: 20,),
            hideButton? Container():InkWell(
                onTap: (){
                  SelectDesignViewModel.of(context).updateSelectedComp((design.id??"").toString());
                },
                child: Image.asset("assets/images/add_cart.png",scale: 3,)),

          ],
        ),
      ],
    );
  }
}
