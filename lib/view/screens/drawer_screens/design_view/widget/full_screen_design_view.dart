


import 'package:colurama/app_manager/components/colored_safe_area.dart';
import 'package:colurama/app_manager/constant/project_constant.dart';
import 'package:colurama/app_manager/helper/file_from_image_url.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:share_plus/share_plus.dart';

class FullScreenDesignView extends StatelessWidget {

  final Design design;

  const FullScreenDesignView({super.key, required this.design});

  @override
  Widget build(BuildContext context) {

    List<String> imageList = [design.thumbnailPath.toString()];
    final theme=Theme.of(context);
    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: AppColor.black,
        appBar: AppBar(
          backgroundColor: AppColor.black,
          automaticallyImplyLeading: false,
          title: Text("${1}/${imageList.length}",
            style: theme.textTheme.titleMedium!.copyWith(
                color: AppColor.white
            ),),
          actions: [
            InkWell(
              onTap: (){
                MyNavigator.pop(context);
              },
              child: Image.asset("assets/images/cross_white.png",
                scale: 3,),
            ),
          ],
        ),
        // Implemented with a PageView, simpler than setting it up yourself
        // You can either specify images directly or by using a builder as in this tutorial
        body: Column(
          children: [
            Expanded(
              child: PhotoViewGallery.builder(
               // pageController: _pageController,
                itemCount: imageList.length,
                builder: (context, index) {
                  return PhotoViewGalleryPageOptions(
                    imageProvider: NetworkImage(
                      imageList[index],
                    ),
                    // Contained = the smallest possible size to fit one dimension of the screen
                     minScale: PhotoViewComputedScale.contained * 1.5,
                    // // Covered = the smallest possible size to fit the whole screen
                    // maxScale: PhotoViewComputedScale.covered * 2,
                  );
                },
                // Set the background color to the "classic white"
                backgroundDecoration: const BoxDecoration(
                  color: AppColor.black,
                ),
                loadingBuilder: (context, ice){
                  return ProjectConstant.placeHolder;
                },

                onPageChanged: (index){

                },
              ),
            ),

            Container(
              color: AppColor.white,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 20,20,20),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(design.title??"",
                              style: theme.textTheme.titleSmall!.copyWith(
                                  color: AppColor.greyDark
                              ),),
                            Text("Edgeband: ${design.edgeband?.name??""}",
                              style: theme.textTheme.titleSmall!.copyWith(
                                  color: AppColor.greyDark,
                                fontSize: 12
                              ),),
                          ],
                        ),
                      ),
                      const SizedBox(width: 15,),
                      InkWell(
                        onTap: () async{
                          await Share.shareXFiles([XFile((await fileFromImageUrl(design.thumbnailPath??"")).path) ], text: design.title??"");
                        },
                        child: Column(
                          children:  [
                            const Icon(Icons.share,
                            color: AppColor.primaryColor,),
                            Text('IMAGE',
                            style: theme.textTheme.titleSmall!.copyWith(
                              color: AppColor.primaryColor,
                              fontSize: 12
                            ),)
                          ],
                        ),
                      ),
                      const SizedBox(width: 15,),
                      InkWell(
                        onTap: () async{
                          await Share.share(
                            "Laminated selected from Greenlam\n"
                                "Colourama APP\n\n"
                                "Design: ${design.title}\n\n"
                                "Design No: ${design.designNo}\n\n"
                                "Edgeband: ${design.edgeband?.name??""}"
                          );
                        },
                        child: Column(
                          children:  [
                            const Icon(Icons.share,
                              color: AppColor.primaryColor,),
                            Text('DESIGN',
                              style: theme.textTheme.titleSmall!.copyWith(
                                  color: AppColor.primaryColor,
                                  fontSize: 12
                              ),)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}