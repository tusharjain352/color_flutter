
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/view/widget/designs_widget.dart';
import 'package:colurama/view_model/wishlist_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WishListView extends StatelessWidget {
  const WishListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    return Stack(
      children: [
        Selector<WishListViewModel, List<Design>>(
            shouldRebuild: (prev, nex) => true,
            selector: (buildContext, vm) => vm.getDesigns,
            builder: (context,List<Design> data, child) {

              if(data.isEmpty){
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset("assets/images/empty-wishlist.png",
                      scale: 1.5,),
                      Text("Your Wishlist is empty.",
                      style: theme.textTheme.titleMedium!.copyWith(
                        color: Colors.grey.shade700
                      ),)
                    ],
                  ),
                );
              }
              else{
                return DesignsWidget(designs: data);
              }
          }
        ),

        Positioned(
            bottom: 25,
            right: 25,
            child: InkWell(onTap: (){
              WishListViewModel.of(context).clearAll();

            }, child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: AppColor.primaryColor
                ),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    offset: const Offset(1,3),
                    spreadRadius: 1
                  )
                ]
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0,15,0,15),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(width: 15,),
                    Image.asset("assets/images/delete_sweep_black.png",
                    scale: 2,),
                    const SizedBox(width: 10,),
                    Text("CLEAR WISHLIST",
                    style: theme.textTheme.titleSmall!.copyWith(
                      fontWeight: FontWeight.normal,
                      fontSize: 8
                    ),),
                    const SizedBox(width: 15,),
                  ],
                ),
              ),
            )))
      ],
    );
  }
}
