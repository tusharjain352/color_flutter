import 'package:colurama/app_manager/helper/my_url_luancher.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class AboutUsView extends StatefulWidget {
  const AboutUsView({Key? key}) : super(key: key);

  @override
  State<AboutUsView> createState() => _AboutUsViewState();
}

class _AboutUsViewState extends State<AboutUsView> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    const String text =
        "Greenlam Laminates is among the world's top 3, Asia's largest and India's No. 1 surfacing solutions brand. With its presence in over 100 countries, Greenlam focuses on developing superior quality product with great passion for innovation. It offers end to end surfacing solutions spread across laminates, compacts, interior and exterior cladding and restroom cubicles and locker solutions.Manufactured in two state of the art manufacturing facilities, Greenlam is the first choice when it comes to architects, interior designers and home owners for transforming and creating an exceptional living space.";

    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset("assets/images/back8.jpg"),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text.toString(),
                  style: theme.textTheme.bodyMedium!.copyWith(height: 1.5),
                ),
                const SizedBox(
                  height: 30,
                ),
                Text(
                  "OUR Certifications",
                  style: theme.textTheme.titleMedium!
                      .copyWith(fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      child: certificates(context,
                          title: "Ani-Bacterial", asset: "Anti-bacterial"),
                    ),
                    Expanded(
                      child: certificates(context,
                          title: "Greenguard", asset: "Greenguard"),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Expanded(
                      child: certificates(context,
                          title: "Greenguard Gold", asset: "Greenguard-gold"),
                    ),
                    Expanded(
                      child: certificates(context, title: "NSF", asset: "NSF"),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Expanded(
                      child: certificates(context, title: "FSC", asset: "FSC"),
                    ),
                    Expanded(
                      child: certificates(context,
                          title: "GreenLabel", asset: "Greenlable"),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 60,
                ),
                Text(
                  "For more information".toString(),
                  style: theme.textTheme.bodyMedium,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "visit ".toString(),
                        style: theme.textTheme.bodyMedium,
                      ),
                      TextSpan(
                          text: 'www.greenlam.com',
                          style: theme.textTheme.bodyMedium!.copyWith(
                              color: AppColor.secondaryColor,
                              fontWeight: FontWeight.normal),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              await MyUrlLauncher.launchInBrowser(
                                  'https://www.greenlam.com/');
                            }),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "email ".toString(),
                        style: theme.textTheme.bodyMedium,
                      ),
                      TextSpan(
                          text: 'info@greenlam.com',
                          style: theme.textTheme.bodyMedium!.copyWith(
                              color: AppColor.secondaryColor,
                              fontWeight: FontWeight.normal),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              MyUrlLauncher.launchMail('info@greenlam.com');
                            }),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: "sms".toString(),
                        style: theme.textTheme.bodyMedium!.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 12
                        ),
                      ),
                      TextSpan(children: [
                        TextSpan(
                            text: " <GREENLAM> ".toString(),
                            style: theme.textTheme.bodyLarge!.copyWith(
                              fontWeight: FontWeight.bold,
                                fontSize: 12
                            )),
                        TextSpan(
                            text: "at 53030".toString(),
                            style: theme.textTheme.bodyMedium!.copyWith(
                                fontWeight: FontWeight.bold,
                              fontSize: 12
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                MyUrlLauncher.launchSMS(
                                    number: "53030", body: "GREENLAM");
                              }),
                      ]),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget certificates(BuildContext context,
      {required String title, required String asset}) {
    final theme = Theme.of(context);
    return Column(
      children: [
        Image.asset("assets/images/$asset.png"),
        const SizedBox(
          height: 5,
        ),
        Text(
          title.toString(),
          style: theme.textTheme.bodyMedium,
        ),
      ],
    );
  }
}
