import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:flutter/material.dart';


class FaqView extends StatelessWidget {
  const FaqView({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    String yourText = 'YOUR';
    String colorText1 = '\tCOLOUR';
    String colorText2 = '\tLAMINATE';
    String faqParagraph =
        "Greenlam Colourama app is every Architects and interior designers go-to app. In just a few seconds, you can turn your inspiration into reality. Simple, 'Click' any image and select your favorite colour or 'Pick' colour from palette and we will match it to our laminate. you can also find complimentary laminate and edgeband suggestions.";

    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset('assets/images/back7.jpg'),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        yourText.toString(),
                        style: theme.textTheme.headlineSmall!.copyWith(
                            //color: AppColor.maroon,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        colorText1.toString(),

                        style: theme.textTheme.headlineSmall!.copyWith(
                            color: AppColor.maroon,
                            fontWeight: FontWeight.bold),

                        //style: theme.textTheme.headline4,
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        yourText.toString(),
                        style: theme.textTheme.headlineSmall!
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        colorText2.toString(),
                        style: theme.textTheme.headlineSmall!.copyWith(
                            color: AppColor.maroon,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  const SizedBox(height: 20.0),
                  Text(
                    faqParagraph.toString(),
                    style: theme.textTheme.bodyMedium,
                  ),
                ]),
          )
        ],
      ),
    );
  }
}
