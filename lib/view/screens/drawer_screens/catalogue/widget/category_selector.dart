

import 'package:colurama/model/category.dart';
import 'package:colurama/view_model/catalogue_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategorySelector extends StatelessWidget {
  final List<CategoryData> categories;
  const CategorySelector({Key? key, required this.categories}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme=Theme.of(context);
    final CatalogueVM vm=CatalogueVM.of(context);
    return ListView.builder(
      shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: categories.length,
        itemBuilder: (context,index){
          CategoryData category=categories[index];
          return InkWell(
            onTap: (){
              vm.addOrRemoveCategory=category;
            },
            child: Container(
              decoration: const BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                      color: Colors.grey
                    )
                )
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20,0,0,0,),
                      child: Text(category.code??"",
                      style: theme.textTheme.titleMedium!.copyWith(
                        color: Colors.grey.shade700
                      ),),
                    ),
                  ),
                  const SizedBox(width: 10,),
                  Text('( ${category.designsCount??""} Designs )',
                  style: theme.textTheme.titleSmall!.copyWith(
                    color: Colors.grey.shade700,
                    fontWeight: FontWeight.normal
                  ),),
                  Selector<CatalogueVM,List<CategoryData>>(
                      shouldRebuild: (prev, nex) => true,
                      selector: (buildContext, vm) => vm.selectedCategory,
                      builder: (context,List<CategoryData> data, child) {
                        bool selected=data.map((e) => e.code).contains(category.code??"");
                      return Checkbox(
                          activeColor: Colors.grey.shade700,
                          value: selected, onChanged: (val){
                        vm.addOrRemoveCategory=category;
                      });
                    }
                  )
                ],
              ),
            ),
          );
        });
  }
}
