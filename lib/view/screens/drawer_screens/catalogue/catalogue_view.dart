// ignore_for_file: invalid_use_of_protected_member
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/api/manage_response.dart';
import 'package:colurama/app_manager/components/bottom_sheet/custom_bottom_sheet.dart';
import 'package:colurama/app_manager/components/bottom_sheet/titled_sheet.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/model/category.dart';
import 'package:colurama/view/screens/drawer_screens/catalogue/widget/category_selector.dart';
import 'package:colurama/view/widget/button_border.dart';
import 'package:colurama/view/widget/designs_widget.dart';
import 'package:colurama/view_model/catalogue_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class Catalogue extends StatefulWidget {
  const Catalogue({super.key});

  @override
  State<Catalogue> createState() => _CatalogueState();
}

class _CatalogueState extends State<Catalogue> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get();
  }

  get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      CatalogueVM.of(context).initiateCatalogue(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    CatalogueVM vm=CatalogueVM.of(context);


    Widget floatingButton = ButtonBorder(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: (){
              CustomBottomSheet.open(context,
              child:    Selector<CatalogueVM,Tuple2<ApiResponse<List<CategoryData>>,List<CategoryData>>>(
                  shouldRebuild: (prev, nex) => true,
                  selector: (buildContext, vm) => Tuple2(vm.categoryResponse, vm.selectedCategory),
                  builder: (context,Tuple2<ApiResponse<List<CategoryData>>,List<CategoryData>> data, child) {
                    bool selected=data.item2.length==(data.item1.data??[]).length;

                  return ManageResponse(response: data.item1, child: TitledSheet(
                      bodyPadding: const EdgeInsets.all(0),
                      titlePadding: const EdgeInsets.fromLTRB(10,0,0,0,),
                      action:[
                        Text(selected? "Deselect All":"Select All",
                          style: theme.textTheme.titleSmall!.copyWith(
                              color: Colors.grey.shade700,
                              fontWeight: FontWeight.normal
                          ),),
                        Checkbox(
                            activeColor: Colors.grey.shade700,
                            value: selected, onChanged: (val){
                          vm.selectOrDeSelectAll();
                        })
                      ],
                      title: "Categories", child: CategorySelector(
                    categories: data.item1.data??[],
                  )));
                }
              ));
            },
            child: Image.asset(
              "assets/images/filter_blue.png",
              height: 25,
              width: 40,
            ),
          ),
          const SizedBox(
            width: 8,
          ),
          const VerticalDivider(
            color: AppColor.black,
            thickness: 0.8,
          ),
          const SizedBox(
            width: 8,
          ),
          Expanded(
            child: TextFormField(
              controller: vm.searchC,
              style: const TextStyle(fontSize: 22),
              decoration: const InputDecoration(
                hintText: 'Search',
              ),
              onChanged: (val){
                // ignore: invalid_use_of_visible_for_testing_member
                vm.notifyListeners();
              },
            ),
          )
        ],
      ),
    );

    return Column(
      children: [
        Expanded(
          child: Selector<CatalogueVM, Tuple2<ApiResponse<List<Design>>,List<Design>>>(
              shouldRebuild: (prev, nex) => true,
              selector: (buildContext, vm) => Tuple2(vm.catalogueResponse, vm.designs),
              builder: (context,Tuple2<ApiResponse<List<Design>>,List<Design>> data,
                  child) {
                List<Design> designs=data.item2;
              return ManageResponse(
                response: data.item1,
                onPressRetry: (){
                  vm.fetchCatalogue(context);
                },
                child: DesignsWidget(designs:designs),
              );
            }
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: floatingButton,
        )
      ],
    );
  }
}
