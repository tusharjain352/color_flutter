import 'package:colurama/app_manager/extension/is_valid_email.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:colurama/view_model/contact_us_view_model.dart';
import 'package:flutter/material.dart';


class ContactUsView extends StatefulWidget {
  const ContactUsView({super.key});

  @override
  State<ContactUsView> createState() => _ContactUsViewState();
}

class _ContactUsViewState extends State<ContactUsView> {


  final GlobalKey<FormState> formKey = GlobalKey<FormState>();




  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    get();
  }


  void get() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

      ContactUsVM.of(context).initiateCS(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    ContactUsVM vm = ContactUsVM.of(context);
    return SingleChildScrollView(
        child: Column(
      children: [
        Image.asset('assets/images/back3.jpg'),
        Padding(
          padding: const EdgeInsets.fromLTRB(20,40,20,20,),
          child: Form(
              key: formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Your name',
                        style: theme.textTheme.titleSmall!
                            .copyWith(color: AppColor.primaryColor)),
                    TextFormField(
                      controller: vm.nameC,
                      decoration: const InputDecoration(
                          hintText: "NAME"
                      ),
                      validator: (String? val) {
                        if (val == null || val.trim().isEmpty) {
                          return "Required field";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(height: 30),
                    Text('Your mobile',
                        style: theme.textTheme.titleSmall!
                            .copyWith(color: AppColor.primaryColor)),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      maxLength: 10,
                      controller: vm.numberC,
                      decoration: const InputDecoration(
                        hintText: "MOBILE",
                        counterText: "",
                      ),
                      validator: (String? val) {
                        if (val == null || val.trim().isEmpty) {
                          return "Required field";
                        } else  if (val.length<10) {
                          return "Please enter a valid 10 digit mobile number";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(height: 30),
                    Text('Your email',
                        style: theme.textTheme.titleSmall!
                            .copyWith(color: AppColor.primaryColor)),
                    TextFormField(
                      controller: vm.emailC,
                      keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                          hintText: "EMAIL"
                      ),
                      validator: (String? val) {
                        if (val == null || val.trim().isEmpty) {
                          return "Please enter a valid email address";
                        }
                        else  if (!val.isValidEmail()) {
                          return "Please enter a valid email address";
                        }
                        else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {

                          if(!vm.emailC.text.isValidEmail()){
                            Alert.showDialogue(context, "Please enter a valid email address");
                          }
                          else {
                            if(formKey.currentState!.validate()){
                              vm.onPressSubmit(context);
                            }
                            else {
                              Alert.show("Fields are not validated");
                            }
                          }

                        },
                        child: const Text('SUBMIT'),
                      ),
                    )
                  ])),
        ),
      ],
    ));
  }
}
