import 'package:flutter/material.dart';

class DisclaimerView extends StatefulWidget {
  const DisclaimerView({super.key});

  @override
  State<DisclaimerView> createState() => _DisclaimerViewState();
}

class _DisclaimerViewState extends State<DisclaimerView> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    const String teext =
        'This product swatches shown here are only for representation purpose. Due to techinical limitations actual product might vary. Please refer to physical sample for colour & texture.';
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset('assets/images/back4.jpg'),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  teext.toString(),
                  style: theme.textTheme.bodyMedium,
                  // theme.textTheme.titleMedium!
                  //     .copyWith(fontWeight: FontWeight.w500),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
