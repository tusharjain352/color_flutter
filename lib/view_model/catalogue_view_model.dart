import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/model/category.dart';
import 'package:colurama/view_model/country_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';



class CatalogueVM extends ChangeNotifier {
  static CatalogueVM of(BuildContext context) => Provider.of<CatalogueVM>(context, listen: false);

  final ApiCall _api = ApiCall();

  final TextEditingController searchC = TextEditingController();


  void initiateCatalogue(context) {
    selectedCategory.clear();
     fetchCatalogue(context);
     fetchCategories(context);
  }


  ApiResponse<List<Design>> _catalogueResponse =
      ApiResponse<List<Design>>.initial("");
  ApiResponse<List<Design>> get catalogueResponse => _catalogueResponse;
  set catalogueResponse(ApiResponse<List<Design>> res) {
    _catalogueResponse = res;
    notifyListeners();
  }

  List<Design> get _designs=>catalogueResponse.data??[];
  List<Design> get _filtered=>_designs.where((element) =>
  selectedCategory.map((e) => e.id.toString()).toList().contains(element.categoryId)
  ).toList();
  List<Design> get designs=>searchC.text.trim().isEmpty?_filtered:
  _filtered.where((design) => design.title.toString()
      .toLowerCase().contains(searchC.text.toLowerCase())).toList();



  int getCategoryId(String category) {
    return (categoryResponse.data??[]).firstWhere((element) => element.name==category).id??0;

  }



  void selectOrDeSelectAll(){
    if(selectedCategory.length!=allCategories.length){
      selectedCategory.clear();
      selectedCategory.addAll(allCategories);
    }

    else {
      selectedCategory.clear();
    }
    notifyListeners();
  }


  List<CategoryData> selectedCategory=[];
  List<String> get selectedCategoryAsString=>selectedCategory.map((e) => e.code??"").toList();
  set addOrRemoveCategory(CategoryData cat) {
    if(selectedCategoryAsString.contains(cat.code??"")){
      selectedCategory.removeWhere((element) => element.code==cat.code);
    }
    else {
      selectedCategory.add(cat);
    }
    notifyListeners();
  }


  Future<void> fetchCatalogue(context) async {
    if ((catalogueResponse.data ?? []).isEmpty) {
      catalogueResponse = ApiResponse<List<Design>>.loading('Fetching Catalogue');
    }
    try {
      String countryId=CountryVM.of(context).getSelectedCountry.id.toString();
      var data = await _api.call(
        context,
        url: "/designs/?offset=0&country=$countryId",
        apiCallType: ApiCallType.get(),
      );

      if (((data ?? []) as List).isNotEmpty) {
        catalogueResponse = ApiResponse<List<Design>>.completed(
            List<Design>.from(
                (data as List).map((e) => Design.fromJson(e))));
      } else {
        catalogueResponse =
            ApiResponse<List<Design>>.empty("No Data Available");
      }
    } catch (e) {
      catalogueResponse = ApiResponse<List<Design>>.error(e.toString());
    }
  }


  ApiResponse<List<CategoryData>> _categoryResponse =
  ApiResponse<List<CategoryData>>.initial("");
  ApiResponse<List<CategoryData>> get categoryResponse => _categoryResponse;

  List<CategoryData> get allCategories=>(categoryResponse.data??[]);
  set categoryResponse(ApiResponse<List<CategoryData>> res) {
    _categoryResponse = res;
    notifyListeners();
  }

  Future<void> fetchCategories(context) async {
    if ((catalogueResponse.data ?? []).isEmpty) {
      categoryResponse = ApiResponse<List<CategoryData>>.loading('Fetching Categories');
    }
    try {
      var data = await _api.call(
        context,
        url: "/categories",
        apiCallType: ApiCallType.get(),
      );

      if (((data ?? []) as List).isNotEmpty) {

        categoryResponse = ApiResponse<List<CategoryData>>.completed(
            List<CategoryData>.from(
                (data as List).map((e) => CategoryData.fromJson(e))));

        selectedCategory.addAll(allCategories);
      } else {
        categoryResponse =
        ApiResponse<List<CategoryData>>.empty("No Data Available");
      }
    } catch (e) {
      categoryResponse = ApiResponse<List<CategoryData>>.error(e.toString());
    }
  }


}
