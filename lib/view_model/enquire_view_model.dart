import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/helper/progress_dialogue.dart';
import 'package:colurama/authentication/user_repository.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/model/country.dart';
import 'package:colurama/model/user.dart';
import 'package:colurama/routes.dart';
import 'package:colurama/view_model/country_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EnquireViewModel  extends ChangeNotifier {

  static EnquireViewModel of(BuildContext context)=>Provider.of<EnquireViewModel>(context,listen: false);

  final ApiCall _api=ApiCall();


  void initiate(BuildContext context){
    CountryVM cv=CountryVM.of(context);
    Country country=cv.getSelectedCountry;
    if(country.id.toString()=="1"){
      cv.initiateStateFetching(context);
    }

    nameC.clear();
    numberC.clear();
    poBoxC.clear();
    emailC.clear();
    pincodeC.clear();
    countryC.clear();
    cityC.clear();

    User user=UserRepository.of(context).getUser;

    nameC.text=user.name??"";
    numberC.text=user.mobile??"";
    emailC.text=user.email??"";

    countryC.text=country.name??"";

  }


  // variables




  final TextEditingController nameC=TextEditingController();
  final TextEditingController numberC=TextEditingController();
  final TextEditingController emailC=TextEditingController();
  final TextEditingController pincodeC=TextEditingController();
  final TextEditingController countryC=TextEditingController();
  final TextEditingController cityC=TextEditingController();
  final TextEditingController poBoxC=TextEditingController();



  Future<void> onPressSubmit(BuildContext context,{
    required Design design,
    required List<String> selectedComplement,
  }) async {

    CountryVM cv=CountryVM.of(context);
    bool isIndia=cv.getSelectedCountry.id==1;

    if(!isIndia){
      _submit(context,
          design: design,
          selectedComplement: selectedComplement);
    }
    else {
      if(cv.selectState==null){
        Alert.showDialogue(context,"Select State");
      }
      else if(cv.selectCity==null){
        Alert.showDialogue(context,"Select City");
      }
      else {
        _submit(context,
            design: design,
            selectedComplement: selectedComplement);
      }
    }



  }

  Future<void> _submit(BuildContext context,{
     required Design design,
    required List<String> selectedComplement,
}) async {
    PD.show(context, message: "");
    try {

      String query="This ${nameC.text} Hi, I am interested in the Design ";

      if(selectedComplement.contains(design.complementaryDesign1?.id.toString())){
        query="${query}with complementary design 1 ";
      }
      if(selectedComplement.contains(design.complementaryDesign2?.id.toString())){
        query="${query}and with complementary design 2 ";
      }
      query="${query}Can you please get me quote for the same";

      CountryVM cv=CountryVM.of(context);

      bool isIndia=cv.getSelectedCountry.id==1;
      String city=!isIndia? cityC.text:cv.selectCity!.name.toString();

      var data= await _api.call(context,
          url: "/contactus?name=${nameC.text}&mobile=${numberC.text}&email=${emailC.text}"
              "pincode=${pincodeC.text}&purpose=GET YOUR LAMINATE&query="
              "$query"
              "&query_type=GENERAL"
              "&country=${countryC.text.toString()}"
              "&state=${cv.selectState!.name}"
              "&city=$city"
              "&pobox=${poBoxC.text}"
              "&laminates= ${design.title} ${design.designName}"
              "&City_International",
          apiCallType: ApiCallType.get()
      );
      PD.hide();
      if(data['type']=="success"){
        // ignore: use_build_context_synchronously
        MyNavigator.pushAndRemoveUntil(context, Routes.home);
        // ignore: use_build_context_synchronously
        Alert.showDialogue(context,data['message']);
      }
      else {
        Alert.show(data.toString());
      }
    }
    catch (e){
      PD.hide();
    }
  }


}