




import 'dart:convert';

import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/constant/storage_constant.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/model/country.dart';
import 'package:colurama/model/state_data.dart';
import 'package:colurama/view_model/drawer_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_manager/theme/widget_theme_data/custon_button_theme.dart';

class CountryVM extends ChangeNotifier {

  Country? currentCountry;

  CountryVM({
    this.currentCountry,

  });


  Country get getSelectedCountry=>currentCountry??Country();

  Future updateSelectedCountry(Country country) async{
    String con=jsonEncode(country.toJson());
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(StorageConstant.selectedCountryStore,con);
    currentCountry=await fetchCurrentCountry();
    notifyListeners();
  }


  Future<void> updateSCAndMoveToHome(BuildContext context,Country country) async {

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Select Country"),
          content: Text("Are you sure you want to choose ${country.name??""} as your country?"),
          actions: [
            TextButtonTheme(
              data: CustomButtonTheme.text,
              child: TextButton(onPressed: (){
                MyNavigator.pop(context);
              }, child: const Text("CANCEL")),
            ),
            TextButtonTheme(
              data: CustomButtonTheme.text,
              child: TextButton(onPressed: (){
                updateSelectedCountry(country).then((value) =>
                    MyNavigator.pushReplacement(context, DrawerVM.homeRoute)
                );
              }, child: const Text("OK")),
            ),
          ],
        );
      },
    );
  }


  static Future<Country> fetchCurrentCountry() async{
    final prefs = await SharedPreferences.getInstance();
    return Country.fromJson(jsonDecode(prefs.getString(StorageConstant.selectedCountryStore)??"{}"));
  }


  static CountryVM of(BuildContext context)=>Provider.of<CountryVM>(context,listen: false);

  final ApiCall _api=ApiCall();




  ApiResponse<List<Country>> _countryResponse =
  ApiResponse<List<Country>>.initial("");
  ApiResponse<List<Country>> get countryResponse => _countryResponse;
  set countryResponse(ApiResponse<List<Country>> res) {
    _countryResponse = res;
    notifyListeners();
  }

  void fetchCountries(context) async {
    if((countryResponse.data??[]).isEmpty){
      countryResponse = ApiResponse<List<Country>>.loading('Fetching Country');
    }
    try {
      var data = await _api.call(
        context,
          url: "/countries",
        apiCallType: ApiCallType.get(),
      );

        if (((data ?? []) as List).isNotEmpty) {
          countryResponse = ApiResponse<List<Country>>.completed(
              List<Country>.from(
                  (data as List).map((e) => Country.fromJson(e))));

          if(getSelectedCountry.id==null){
            updateSelectedCountry((countryResponse.data??[])[0]);
          }
        } else {
          countryResponse = ApiResponse<List<Country>>.empty("No Data Available");
        }
    } catch (e) {
      countryResponse = ApiResponse<List<Country>>.error(e.toString());
    }
  }





  ApiResponse<List<StateData>> _stateResponse =
  ApiResponse<List<StateData>>.initial("");
  ApiResponse<List<StateData>> get stateResponse => _stateResponse;
  set stateResponse(ApiResponse<List<StateData>> res) {
    _stateResponse = res;
    notifyListeners();
  }


  StateData? _selectState;
  StateData? get selectState=>_selectState;
  set selectState(StateData? state) {
    _selectState=state;
    notifyListeners();
  }



  Cities? _selectCity;
  Cities? get selectCity=>_selectCity;
  set selectCity(Cities? city) {
    _selectCity=city;
    notifyListeners();
  }

  void initiateStateFetching(context){
    selectState=null;
    selectCity=null;
    fetchStates(context);
  }

  void fetchStates(context) async {
    if((stateResponse.data??[]).isEmpty){
      stateResponse = ApiResponse<List<StateData>>.loading('Fetching States');
    }
    try {
      var data = await _api.call(
        context,
        url: "/states",
        apiCallType: ApiCallType.get(),
      );

      if (((data ?? []) as List).isNotEmpty) {
        stateResponse = ApiResponse<List<StateData>>.completed(
            List<StateData>.from(
                (data as List).map((e) => StateData.fromJson(e))));
      } else {
        stateResponse = ApiResponse<List<StateData>>.empty("No Data Available");
      }
    } catch (e) {
      stateResponse = ApiResponse<List<StateData>>.error(e.toString());
    }
  }

}