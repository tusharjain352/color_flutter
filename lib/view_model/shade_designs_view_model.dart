import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/view_model/country_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class ShadeDesignsVM extends ChangeNotifier {

  static ShadeDesignsVM of(BuildContext context)=>Provider.of<ShadeDesignsVM>(context,listen: false);

  final ApiCall _api=ApiCall();

  ApiResponse<List<Design>> _designsResponse =
  ApiResponse<List<Design>>.initial("");
  ApiResponse<List<Design>> get designsResponse => _designsResponse;
  set designsResponse(ApiResponse<List<Design>> res) {
    _designsResponse = res;
    notifyListeners();
  }

  Future<void> fetchDesigns(context,{
    required String shadeHex
  }) async {

      designsResponse = ApiResponse<List<Design>>.loading('Fetching Designs');
    try {
      String countryId=CountryVM.of(context).getSelectedCountry.id.toString();
      var data = await _api.call(
        context,
        url: "/designs/?offset=0&color=${shadeHex.replaceAll('#', "").toUpperCase()}&threshold=0&country=$countryId",
        apiCallType: ApiCallType.get(),
      );



      if (((data ?? []) as List).isNotEmpty) {
        designsResponse = ApiResponse<List<Design>>.completed(
            List<Design>.from(
                (data as List).map((e) => Design.fromJson(e))));
      } else {
        designsResponse = ApiResponse<List<Design>>.empty("No Data Available");
      }
    } catch (e) {
      designsResponse = ApiResponse<List<Design>>.error(e.toString());
    }
  }




}