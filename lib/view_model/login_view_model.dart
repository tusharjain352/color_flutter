





import 'dart:developer';

import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/helper/progress_dialogue.dart';
import 'package:colurama/app_manager/theme/widget_theme_data/custon_button_theme.dart';
import 'package:colurama/authentication/user_repository.dart';
import 'package:colurama/model/user.dart';
import 'package:colurama/view_model/drawer_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginVM extends ChangeNotifier {

  static LoginVM of(BuildContext context)=>Provider.of<LoginVM>(context,listen: false);


  void initiateLoginScreen(){
    nameC.clear();
    numberC.clear();
    _readPolicy=false;
  }


  // variables


  final ApiCall _api=ApiCall();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController nameC=TextEditingController();
  final TextEditingController numberC=TextEditingController();

  bool _readPolicy=false;
  bool get readPolicy=>_readPolicy;
  set readPolicy(bool val) {
    _readPolicy=val;
    notifyListeners();
    checkToEnableButton();
  }


  bool _enableButton=false;
  bool get enableButton=>_enableButton;
  void checkToEnableButton() {
    if(formKey.currentState!.validate() && readPolicy){
      _enableButton=true;
    }
    else {
      _enableButton=false;
    }
    notifyListeners();
  }



  void onPressNext(BuildContext context){
    if(formKey.currentState!.validate() && readPolicy){
      _registerUser(context,
      mobile: numberC.text,
      name: nameC.text,
    );
    }
    else {

    }
  }



  Future<void> _registerUser(BuildContext context,{
    required String name,
    required String mobile,
}) async{
    PD.show(context, message: "Registering User");
    try {
      var data=await _api.call(context,
          url: "/register?name=$name&mobile=$mobile",
          apiCallType: ApiCallType.get()
      );
      PD.hide();

      log(data.toString());


      if(data['type']=="success"){

        if(data['message']=="User registered successfully."){
         // ignore: use_build_context_synchronously
          _registerUser(context, name: name, mobile: mobile).then((value) =>
            UserRepository.of(context).updateUserData(
              User.fromJson(data['user'])
            ).then((value) {
              nameC.clear();
              numberC.clear();
              MyNavigator.pushAndRemoveUntil(context,  DrawerVM.primaryRoute);
            }
            )
          );
        }
        else {
          // ignore: use_build_context_synchronously
          UserRepository.of(context).updateUserData(
              User.fromJson(data['user'])
          ).then((value) {
            nameC.clear();
            numberC.clear();
            MyNavigator.pushAndRemoveUntil(context,  DrawerVM.primaryRoute);
          });
        }

      }
      else {
        Alert.show(data['message']);
      }
    }
    catch (e) {
      PD.hide();
    }

  }



  Future<void> onPressDeleteUser(BuildContext context) async{
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Delete Account"),
          content: const Text("Are you sure you want to delete your account?"),
          actions: [
            TextButtonTheme(
              data: CustomButtonTheme.text,
              child: TextButton(onPressed: (){
                MyNavigator.pop(context);
              }, child: const Text("CANCEL")),
            ),
            TextButtonTheme(
              data: CustomButtonTheme.text,
              child: TextButton(onPressed: (){
               _deleteUser(context,);
              }, child: const Text("OK")),
            ),
          ],
        );
      },
    );
  }



  Future<void> _deleteUser(BuildContext context) async{
    User user=UserRepository.of(context).getUser;
    PD.show(context, message: "Deleting User");
    try {
      var data=await _api.call(context,
          url: "/remove-user-account?mobile=${user.mobile.toString()}",
          apiCallType: ApiCallType.delete(body:  {})
      );
      PD.hide();
      if(data['success']){
        // ignore: use_build_context_synchronously
        UserRepository.of(context).logOutUser(context,
        message: "Account Deleted Successfully");
        // ignore: use_build_context_synchronously

      }
      else {
      }
    }
    catch (e) {
      PD.hide();
    }

  }



}