import 'dart:convert';

import 'package:colurama/app_manager/constant/storage_constant.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WishListViewModel extends ChangeNotifier {

  static WishListViewModel of(BuildContext context) => Provider.of<WishListViewModel>(context, listen: false);

  List<Design>? wishList;

  WishListViewModel({
    this.wishList,
  });

  List<Design> get getDesigns => wishList ??[];
  List<String> get ids=>getDesigns.map((e) => e.id.toString()).toList();

  Future addOrRemoveDesign(Design design) async {
    if((wishList??[]).map((e) => e.id.toString()).contains(design.id.toString())){
      wishList?.removeWhere((element) => element.id==design.id);
    }
    else {
      wishList?.add(design);
    }
    _updateWishLIst(wishList);

  }

  void _updateWishLIst(List<Design>? designs) async {
    String cartEncode = jsonEncode((designs??[]).map((e) => e.toJson()).toList());
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(StorageConstant.wishList, cartEncode);
    wishList = await fetWishList();
    notifyListeners();
  }
  static Future<List<Design>> fetWishList() async {
    final prefs = await SharedPreferences.getInstance();
    return jsonDecode(prefs.getString(StorageConstant.wishList) ?? "[]")==null? []:List<Design>.from(
        (jsonDecode(prefs.getString(StorageConstant.wishList) ?? "[]"))
            .map((e) => Design.fromJson(e))
    );
  }

  void clearAll(){
    _updateWishLIst([]);
  }


}