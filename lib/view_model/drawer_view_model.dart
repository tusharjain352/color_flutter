import 'package:colurama/model/drawer_option.dart';
import 'package:colurama/view/screens/drawer_screens/about_us_view.dart';
import 'package:colurama/view/screens/drawer_screens/country_view.dart';
import 'package:colurama/view/screens/drawer_screens/disclaimer_view.dart';
import 'package:colurama/view/screens/drawer_screens/faq_view.dart';
import 'package:colurama/view/screens/drawer_screens/feedback_view.dart';
import 'package:colurama/view/screens/drawer_screens/inspiration_view.dart';
import 'package:colurama/view/screens/drawer_screens/wishlist_view.dart';
import 'package:colurama/view/screens/home_screen_view.dart';
import 'package:colurama/view/widget/drawered_view.dart';
import 'package:colurama/view_model/login_view_model.dart';
import 'package:flutter/cupertino.dart';

import '../view/screens/drawer_screens/catalogue/catalogue_view.dart';
import '../view/screens/drawer_screens/contact_us_view.dart';

class DrawerVM {
  static final DrawerOption _primaryDrawer = DrawerOption(
      title: 'COUNTRY', label: 'Select Country', child: const CountryView());
  static final primaryRoute = DraweredView(
      title: _primaryDrawer.label ?? (_primaryDrawer.title ?? ""),
      child: _primaryDrawer.child ?? Container());

  static final DrawerOption _homeDrawer =
      DrawerOption(title: 'HOME', child: const HomeScreenView());
  static final homeRoute = DraweredView(
      title: _homeDrawer.label ?? (_homeDrawer.title ?? ""),
      child: _homeDrawer.child ?? Container());

  static final List<DrawerOption> drawerOptions = [
    _homeDrawer,
    DrawerOption(title: 'ABOUT US', child: const AboutUsView()),
    DrawerOption(title: 'CATALOGUE', child: const Catalogue()),
    DrawerOption(title: 'INSPIRATIONS', child: const InspirationView()),
    DrawerOption(title: 'WISHLIST',child: const WishListView()),
    DrawerOption(title: 'FAQ', child: const FaqView()),
    DrawerOption(title: 'CONTACT US', child: const ContactUsView()),
    DrawerOption(title: 'FEEDBACK', child: const FeedbackView()),
    DrawerOption(title: 'DISCLAIMER', child: const DisclaimerView()),
    _primaryDrawer,
    DrawerOption(
        title: 'DELETE ACCOUNT',
        onPress: (BuildContext context) {
          LoginVM.of(context).onPressDeleteUser(context);
        }),
  ];
}
