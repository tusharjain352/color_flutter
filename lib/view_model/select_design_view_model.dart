



import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class SelectDesignViewModel extends ChangeNotifier {

  static SelectDesignViewModel of(BuildContext context)=>Provider.of<SelectDesignViewModel>(context,listen: false);

  void initiate(){
    selectedComplementary.clear();
    notifyListeners();
  }





  List<String> selectedComplementary=[];
  void updateSelectedComp(String val){
    if(selectedComplementary.contains(val)){
      selectedComplementary.remove(val);
    }
    else {
      selectedComplementary.add(val);
    }
    notifyListeners();
  }


}