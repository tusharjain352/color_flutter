

import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/model/inspiration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InspirationVM extends ChangeNotifier {


  static InspirationVM of(BuildContext context)=>Provider.of<InspirationVM>(context,listen: false);

  final ApiCall _api=ApiCall();




  ApiResponse<List<Inspiration>> _inspirationResponse =
  ApiResponse<List<Inspiration>>.initial("");
  ApiResponse<List<Inspiration>> get inspirationResponse => _inspirationResponse;
  set inspirationResponse(ApiResponse<List<Inspiration>> res) {
    _inspirationResponse = res;
    notifyListeners();
  }

  void fetchInspirations(context) async {
    if((inspirationResponse.data??[]).isEmpty){
      inspirationResponse = ApiResponse<List<Inspiration>>.loading('Fetching Inspirations');
    }
    try {
      var data = await _api.call(
        context,
        url: "/inspirations",
        apiCallType: ApiCallType.get(),
      );

      if (((data ?? []) as List).isNotEmpty) {
        inspirationResponse = ApiResponse<List<Inspiration>>.completed(
            List<Inspiration>.from(
                (data as List).map((e) => Inspiration.fromJson(e))));
      } else {
        inspirationResponse = ApiResponse<List<Inspiration>>.empty("No Data Available");
      }
    } catch (e) {
      inspirationResponse = ApiResponse<List<Inspiration>>.error(e.toString());
    }
  }


}