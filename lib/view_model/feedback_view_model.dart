
import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/app_manager/helper/progress_dialogue.dart';
import 'package:colurama/authentication/user_repository.dart';
import 'package:colurama/model/user.dart';
import 'package:colurama/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FeedbackVM extends ChangeNotifier {



  static FeedbackVM of(BuildContext context)=>Provider.of<FeedbackVM>(context,listen: false);

  final ApiCall _api=ApiCall();


  void initiateFVM(BuildContext context){
    nameC.clear();
    numberC.clear();
    emailC.clear();
    feedbackC.clear();

    User user=UserRepository.of(context).getUser;

    nameC.text=user.name??"";
    numberC.text=user.mobile??"";
    emailC.text=user.email??"";

  }


  // variables





  final TextEditingController nameC=TextEditingController();
  final TextEditingController numberC=TextEditingController();
  final TextEditingController emailC=TextEditingController();
  final TextEditingController feedbackC=TextEditingController();



  Future<void> onPressSubmit(BuildContext context) async {
    PD.show(context, message: "");
    try {
      var data= await _api.call(context,
          url: "/contactus?name=${nameC.text}&mobile=${numberC.text}&email=${emailC.text}&query=${feedbackC.text}&query_type=test&purpose=FEEDBACK",
          apiCallType: ApiCallType.get()
      );
      PD.hide();
      if(data['type']=="success"){
        // ignore: use_build_context_synchronously
        await MyNavigator.pushAndRemoveUntil(context, Routes.home,
        message: data['message']);
      }
      else {
        Alert.show(data.toString());
      }
    }
    catch (e){
      PD.hide();
    }
  }



}