


import 'package:camera/camera.dart';
import 'package:colurama/app_manager/api/api_call.dart';
import 'package:colurama/app_manager/api/api_response.dart';
import 'package:colurama/app_manager/helper/navigator.dart';
import 'package:colurama/model/color_data.dart';
import 'package:colurama/model/shade.dart';
import 'package:colurama/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SelectShadeVM extends ChangeNotifier {

  static SelectShadeVM of(BuildContext context)=>Provider.of<SelectShadeVM>(context,listen: false);


  List<CameraDescription>? cameras=[];

  SelectShadeVM({
    this.cameras,
  });
  List<CameraDescription> get getCameras=>cameras??[];




  final ApiCall _api=ApiCall();





  Future<void> initiateSS(BuildContext context) async {
    selectedColorData=ColorData();
    selectedShade=null;
    await fetchColors(context);
    // ignore: use_build_context_synchronously
    await fetchShades(context);
  }

  Future<void> initiateIS(BuildContext context) async {
    selectedShade=null;
    selectedFile="";

  }




  String _selectedFile="";
  String get selectedFile=>_selectedFile;
  set selectedFile(String val) {
    _selectedFile=val;
    notifyListeners();
  }




  bool _cameraSelected=false;
  bool get cameraSelected=>_cameraSelected;
  set cameraSelected(bool val) {
    _cameraSelected=val;
    selectedFile="";
    notifyListeners();
  }






  ColorData _selectedColorData=ColorData();
  ColorData get selectedColorData=>_selectedColorData;
  set selectedColorData(ColorData val) {
    if(selectedColorData.id==val.id){
      _selectedColorData=ColorData();
    }
    else{
      _selectedColorData=val;
    }

    notifyListeners();
  }


  ApiResponse<List<ColorData>> _colorsResponse =
  ApiResponse<List<ColorData>>.initial("");
  ApiResponse<List<ColorData>> get colorsResponse => _colorsResponse;
  set colorsResponse(ApiResponse<List<ColorData>> res) {
    _colorsResponse = res;
    notifyListeners();
  }

  Future<void> fetchColors(context) async {
    if((colorsResponse.data??[]).isEmpty){
      colorsResponse = ApiResponse<List<ColorData>>.loading('Fetching Colors');
    }
    try {
      var data = await _api.call(
        context,
        url: "/colors-list",
        apiCallType: ApiCallType.get(),
      );

      if (((data ?? []) as List).isNotEmpty) {
        colorsResponse = ApiResponse<List<ColorData>>.completed(
            List<ColorData>.from(
                (data as List).map((e) => ColorData.fromJson(e))));
      } else {
        colorsResponse = ApiResponse<List<ColorData>>.empty("No Data Available");
      }
    } catch (e) {
      colorsResponse = ApiResponse<List<ColorData>>.error(e.toString());
    }
  }






  String? _selectedShade;
  String? get selectedShade=>_selectedShade;
  set selectedShade(String? val) {
    _selectedShade=val;
    notifyListeners();
  }


  ApiResponse<List<Shade>> _shadeResponse =
  ApiResponse<List<Shade>>.initial("");
  ApiResponse<List<Shade>> get shadeResponse => _shadeResponse;
  List<Shade> get shades=>selectedColorData.id==null? (shadeResponse.data??[]):
  ((shadeResponse.data??[]).where((element) => element.colorGroup==selectedColorData.name)).toList();
  set shadeResponse(ApiResponse<List<Shade>> res) {
    _shadeResponse = res;
    notifyListeners();
  }

  Future<void> fetchShades(context) async {
    if((shadeResponse.data??[]).isEmpty){
      shadeResponse = ApiResponse<List<Shade>>.loading('Fetching Shades');
    }
    try {
      var data = await _api.call(
        context,
        url: "/color-shades",
        apiCallType: ApiCallType.get(),
      );

      if (((data ?? []) as List).isNotEmpty) {
        shadeResponse = ApiResponse<List<Shade>>.completed(
            List<Shade>.from(
                (data as List).map((e) => Shade.fromJson(e))));
      } else {
        shadeResponse = ApiResponse<List<Shade>>.empty("No Data Available");
      }
    } catch (e) {
      shadeResponse = ApiResponse<List<Shade>>.error(e.toString());
    }
  }







  void onPressViewDesign(BuildContext context, String shadeHex){
    MyNavigator.push(context, Routes.shadeDesigns(shadeHex));
  }




}