




import 'dart:convert';
import 'package:colurama/app_manager/helper/alert.dart';
import 'package:colurama/model/user.dart';
import 'package:colurama/view/screens/login_screen_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:colurama/app_manager/constant/storage_constant.dart';
import 'package:colurama/app_manager/helper/navigator.dart';


class UserRepository  extends ChangeNotifier {


  User? currentUser;

  UserRepository({
    this.currentUser,

  });


  User get getUser=>currentUser??User();

  static UserRepository of(BuildContext context)=>Provider.of<UserRepository>(context,listen: false);



  Future updateUserData(User userData) async{
    String user=jsonEncode(userData.toJson());
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(StorageConstant.userStorage,user);
    currentUser=await fetchUserData();
    notifyListeners();
  }


  static Future<User> fetchUserData() async{
    final prefs = await SharedPreferences.getInstance();
    return User.fromJson(jsonDecode(prefs.getString(StorageConstant.userStorage)??"{}"));
  }






  Future logOutUser(BuildContext context,{
    String? message
}) async {
    await updateUserData(User()).then((value) {
      MyNavigator.pushAndRemoveUntil(context, const LoginScreenView());
      if(message!=null){
        Alert.showDialogue(context,message);
      }
    }
    );
    notifyListeners();
  }






}
