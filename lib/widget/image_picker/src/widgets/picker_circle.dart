import 'package:colurama/app_manager/theme/color_constant.dart';
import 'package:flutter/material.dart';

class PickerCircle extends StatelessWidget {
  final AnimationController controller;

  const PickerCircle({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: Tween(begin: 0.75, end: 1.5)
          .animate(CurvedAnimation(
          parent: controller,
          curve: Curves.elasticOut
      )
      ),
      child:  Center(
        child: SizedBox(
          height: 40,
          width: 40,
          child: Center(
            child: Container(
              decoration:  BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: AppColor.primaryColor,
                      width: 2
                  )
              ),
              child: Padding(
                padding: const EdgeInsets.all(2),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.yellow.withOpacity(0.3),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}