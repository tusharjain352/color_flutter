import 'dart:ui' as ui;
import 'package:colurama/widget/image_picker/src/services/pixel_color_picker.dart';
import 'package:colurama/widget/image_picker/src/widgets/picker_circle.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class ImageColorPicker extends StatefulWidget {
  final Widget child;
  final Function(Color color) onChanged;

  const ImageColorPicker({
    Key? key,
    required this.child,
    required this.onChanged,
  }) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _ImageColorPickerState createState() => _ImageColorPickerState();
}

class _ImageColorPickerState extends State<ImageColorPicker>  with TickerProviderStateMixin {
  ColorPicker? _colorPicker;

  final _repaintBoundaryKey = GlobalKey();
  final _interactiveViewerKey = GlobalKey();

  Future<ui.Image> _loadSnapshot() async {
    final RenderRepaintBoundary repaintBoundary =
        _repaintBoundaryKey.currentContext!.findRenderObject() as RenderRepaintBoundary;

    final snapshot = await repaintBoundary.toImage();

    return snapshot;
  }
  Offset localOffsetC=const Offset(0,0);
  _findLocalOffsetForPicker(Offset offset) {
    final RenderBox interactiveViewerBox =
    _interactiveViewerKey.currentContext!.findRenderObject() as RenderBox;

    setState(() {
      localOffsetC = interactiveViewerBox.globalToLocal(offset);
    });
  }
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(duration: const Duration(milliseconds: 200), vsync: this);

  }

  @override
  void dispose(){
    super.dispose();
    _controller.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: _repaintBoundaryKey,
      child: InteractiveViewer(
        key: _interactiveViewerKey,
        maxScale: 10,
        onInteractionStart: (de) {
          _controller.forward();
          setState(() {

          });
        },
        onInteractionEnd: (de) {
          _controller.reverse();
          setState(() {

          });
        },
        onInteractionUpdate: (details) {
          final offset = details.focalPoint;
          _findLocalOffsetForPicker(offset);
          _onInteract(offset);
        },
        child: Stack(
          children: [
            widget.child,
            Positioned(
                left: localOffsetC.dx-20,
                top: localOffsetC.dy-20,
                child:  PickerCircle(
                  controller: _controller,
                ))
          ],
        ),
      ),
    );
  }

  _onInteract(Offset offset) async {
    if (_colorPicker == null) {
      final snapshot = await _loadSnapshot();

      final imageByteData =
          await snapshot.toByteData(format: ui.ImageByteFormat.png);

      final imageBuffer = imageByteData!.buffer;

      final uint8List = imageBuffer.asUint8List();

      _colorPicker = ColorPicker(bytes: uint8List);

      snapshot.dispose();
    }

    final localOffset = _findLocalOffset(offset);

    final color = await _colorPicker!.getColor(pixelPosition: localOffset);

    widget.onChanged(color);
  }

  _findLocalOffset(Offset offset) {
    final RenderBox interactiveViewerBox =
        _interactiveViewerKey.currentContext!.findRenderObject() as RenderBox;

    final localOffset = interactiveViewerBox.globalToLocal(offset);

    return localOffset;
  }
}
