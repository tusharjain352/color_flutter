

class Shade {
  String? color;
  String? colorGroup;
  String? hexcode;
  String? colorDistance;
  String? colorListId;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;
  String? imageSmallPath;
  String? imageMediumPath;
  String? imageLargePath;
  String? imagePortraitPath;

  Shade(
      {this.color,
        this.colorGroup,
        this.hexcode,
        this.colorDistance,
        this.colorListId,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath,
        this.imageSmallPath,
        this.imageMediumPath,
        this.imageLargePath,
        this.imagePortraitPath});

  Shade.fromJson(Map<String, dynamic> json) {
    color = json['color'];
    colorGroup = json['color_group'];
    hexcode = json['hexcode'];
    colorDistance = json['color_distance'];
    colorListId = json['color_list_id'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
    imageSmallPath = json['image_small_path'];
    imageMediumPath = json['image_medium_path'];
    imageLargePath = json['image_large_path'];
    imagePortraitPath = json['image_portrait_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['color'] = color;
    data['color_group'] = colorGroup;
    data['hexcode'] = hexcode;
    data['color_distance'] = colorDistance;
    data['color_list_id'] = colorListId;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    data['image_small_path'] = imageSmallPath;
    data['image_medium_path'] = imageMediumPath;
    data['image_large_path'] = imageLargePath;
    data['image_portrait_path'] = imagePortraitPath;
    return data;
  }
}
