
class Country {
  int? id;
  String? code;
  String? name;
  String? shortName;
  String? thumbnail;
  String? type;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;

  Country(
      {this.id,
        this.code,
        this.name,
        this.shortName,
        this.thumbnail,
        this.type,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    shortName = json['short_name'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['name'] = name;
    data['short_name'] = shortName;
    data['thumbnail'] = thumbnail;
    data['type'] = type;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    return data;
  }
}
