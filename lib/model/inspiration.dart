




class Inspiration {
  int? id;
  String? title;
  String? details;
  String? categoryId;
  String? thumbnail;
  String? imageSmall;
  String? imageMedium;
  String? imageLarge;
  String? status;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;
  String? imageSmallPath;
  String? imageMediumPath;
  String? imageLargePath;
  String? imagePortraitPath;

  Inspiration(
      {this.id,
        this.title,
        this.details,
        this.categoryId,
        this.thumbnail,
        this.imageSmall,
        this.imageMedium,
        this.imageLarge,
        this.status,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath,
        this.imageSmallPath,
        this.imageMediumPath,
        this.imageLargePath,
        this.imagePortraitPath});

  Inspiration.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    details = json['details'];
    categoryId = json['category_id'];
    thumbnail = json['thumbnail'];
    imageSmall = json['image_small'];
    imageMedium = json['image_medium'];
    imageLarge = json['image_large'];
    status = json['status'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
    imageSmallPath = json['image_small_path'];
    imageMediumPath = json['image_medium_path'];
    imageLargePath = json['image_large_path'];
    imagePortraitPath = json['image_portrait_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['details'] = details;
    data['category_id'] = categoryId;
    data['thumbnail'] = thumbnail;
    data['image_small'] = imageSmall;
    data['image_medium'] = imageMedium;
    data['image_large'] = imageLarge;
    data['status'] = status;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    data['image_small_path'] = imageSmallPath;
    data['image_medium_path'] = imageMediumPath;
    data['image_large_path'] = imageLargePath;
    data['image_portrait_path'] = imagePortraitPath;
    return data;
  }
}
