


class ColorData {
  int? id;
  String? name;
  Rgb? rgb;
  Hsl? hsl;
  String? hexcode;
  String? status;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;

  ColorData(
      {this.id,
        this.name,
        this.rgb,
        this.hsl,
        this.hexcode,
        this.status,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn});

  ColorData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    rgb = json['rgb'] != null ? Rgb.fromJson(json['rgb']) : null;
    hsl = json['hsl'] != null ? Hsl.fromJson(json['hsl']) : null;
    hexcode = json['hexcode'];
    status = json['status'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    if (rgb != null) {
      data['rgb'] = rgb!.toJson();
    }
    if (hsl != null) {
      data['hsl'] = hsl!.toJson();
    }
    data['hexcode'] = hexcode;
    data['status'] = status;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    return data;
  }
}

class Rgb {
  int? b;
  int? g;
  int? r;

  Rgb({this.b, this.g, this.r});

  Rgb.fromJson(Map<String, dynamic> json) {
    b = json['b'];
    g = json['g'];
    r = json['r'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['b'] = b;
    data['g'] = g;
    data['r'] = r;
    return data;
  }
}

class Hsl {
  String? h;
  String? l;
  String? s;

  Hsl({this.h, this.l, this.s});

  Hsl.fromJson(Map<String, dynamic> json) {
    h = json['h'].toString();
    l = json['l'].toString();
    s = json['s'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['h'] = h;
    data['l'] = l;
    data['s'] = s;
    return data;
  }
}
