class User {
  String? id;
  String? thumnnail;
  String? name;
  String? mobile;
  String? email;
  String? lastLoginAt;
  String? lastLoginIp;
  String? additional;
  String? status;
  String? createdAt;
  String? updatedAt;

  User(
      {this.id,
        this.thumnnail,
        this.name,
        this.mobile,
        this.email,
        this.lastLoginAt,
        this.lastLoginIp,
        this.additional,
        this.status,
        this.createdAt,
        this.updatedAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    thumnnail = json['thumnnail'];
    name = json['name'];
    mobile = json['mobile'];
    email = json['email'];
    lastLoginAt = json['last_login_at'];
    lastLoginIp = json['last_login_ip'];
    additional = json['additional'];
    status = json['status'].toString();
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['thumnnail'] = thumnnail;
    data['name'] = name;
    data['mobile'] = mobile;
    data['email'] = email;
    data['last_login_at'] = lastLoginAt;
    data['last_login_ip'] = lastLoginIp;
    data['additional'] = additional;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}