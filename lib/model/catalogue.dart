class Design {
  int? id;
  String? designNo;
  String? designName;
  String? finish;
  String? title;
  String? details;
  String? hexcode;
  String? deccode;
  String? type;
  String? color;
  String? colorGroup;
  String? categoryId;
  String? edgebandId;
  String? complementaryDesign1Id;
  String? complementaryDesign2Id;
  String? thumbnail;
  String? imageSmall;
  String? imageMedium;
  String? imageLarge;
  String? imagePortrait;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;
  String? imageSmallPath;
  String? imageMediumPath;
  String? imageLargePath;
  String? imagePortraitPath;
  Edgeband? edgeband;
  ComplementaryDesign1? complementaryDesign1;
  ComplementaryDesign1? complementaryDesign2;

  Design(
      {this.id,
        this.designNo,
        this.designName,
        this.finish,
        this.title,
        this.details,
        this.hexcode,
        this.deccode,
        this.type,
        this.color,
        this.colorGroup,
        this.categoryId,
        this.edgebandId,
        this.complementaryDesign1Id,
        this.complementaryDesign2Id,
        this.thumbnail,
        this.imageSmall,
        this.imageMedium,
        this.imageLarge,
        this.imagePortrait,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath,
        this.imageSmallPath,
        this.imageMediumPath,
        this.imageLargePath,
        this.imagePortraitPath,
        this.edgeband,
        this.complementaryDesign1,
        this.complementaryDesign2});

  Design.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    designNo = json['design_no'];
    designName = json['design_name'];
    finish = json['finish'];
    title = json['title'];
    details = json['details'].toString();
    hexcode = json['hexcode'];
    deccode = json['deccode'].toString();
    type = json['type'].toString();
    color = json['color'];
    colorGroup = json['color_group'];
    categoryId = json['category_id'].toString();
    edgebandId = json['edgeband_id'];
    complementaryDesign1Id = json['complementary_design_1_id'];
    complementaryDesign2Id = json['complementary_design_2_id'];
    thumbnail = json['thumbnail'];
    imageSmall = json['image_small'];
    imageMedium = json['image_medium'];
    imageLarge = json['image_large'];
    imagePortrait = json['image_portrait'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
    imageSmallPath = json['image_small_path'];
    imageMediumPath = json['image_medium_path'];
    imageLargePath = json['image_large_path'];
    imagePortraitPath = json['image_portrait_path'];
    edgeband = json['edgeband'] != null
        ? Edgeband.fromJson(json['edgeband'])
        : null;
    complementaryDesign1 = json['complementary_design1'] != null
        ? ComplementaryDesign1.fromJson(json['complementary_design1'])
        : null;
    complementaryDesign2 = json['complementary_design2'] != null
        ? ComplementaryDesign1.fromJson(json['complementary_design2'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['design_no'] = designNo;
    data['design_name'] = designName;
    data['finish'] = finish;
    data['title'] = title;
    data['details'] = details;
    data['hexcode'] = hexcode;
    data['deccode'] = deccode;
    data['type'] = type;
    data['color'] = color;
    data['color_group'] = colorGroup;
    data['category_id'] = categoryId;
    data['edgeband_id'] = edgebandId;
    data['complementary_design_1_id'] = complementaryDesign1Id;
    data['complementary_design_2_id'] = complementaryDesign2Id;
    data['thumbnail'] = thumbnail;
    data['image_small'] = imageSmall;
    data['image_medium'] = imageMedium;
    data['image_large'] = imageLarge;
    data['image_portrait'] = imagePortrait;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    data['image_small_path'] = imageSmallPath;
    data['image_medium_path'] = imageMediumPath;
    data['image_large_path'] = imageLargePath;
    data['image_portrait_path'] = imagePortraitPath;
    if (edgeband != null) {
      data['edgeband'] = edgeband!.toJson();
    }
    if (complementaryDesign1 != null) {
      data['complementary_design1'] = complementaryDesign1!.toJson();
    }
    if (complementaryDesign2 != null) {
      data['complementary_design2'] = complementaryDesign2!.toJson();
    }
    return data;
  }
}

class Edgeband {
  int? id;
  String? name;
  String? code;
  String? hexcode;
  String? brand;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;
  String? imageSmallPath;
  String? imageMediumPath;
  String? imageLargePath;

  Edgeband(
      {this.id,
        this.name,
        this.code,
        this.hexcode,
        this.brand,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath,
        this.imageSmallPath,
        this.imageMediumPath,
        this.imageLargePath});

  Edgeband.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    code = json['code'];
    hexcode = json['hexcode'].toString();
    brand = json['brand'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
    imageSmallPath = json['image_small_path'].toString();
    imageMediumPath = json['image_medium_path'].toString();
    imageLargePath = json['image_large_path'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['code'] = code;
    data['hexcode'] = hexcode;
    data['brand'] = brand;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    data['image_small_path'] = imageSmallPath;
    data['image_medium_path'] = imageMediumPath;
    data['image_large_path'] = imageLargePath;
    return data;
  }
}

class ComplementaryDesign1 {
  int? id;
  String? designNo;
  String? designName;
  String? finish;
  String? title;
  String? details;
  String? hexcode;
  String? color;
  String? colorGroup;
  String? categoryId;
  String? edgebandId;
  String? complementaryDesign1Id;
  String? complementaryDesign2Id;
  String? thumbnail;
  String? imageSmall;
  String? imageMedium;
  String? imageLarge;
  String? imagePortrait;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;
  String? imageSmallPath;
  String? imageMediumPath;
  String? imageLargePath;
  String? imagePortraitPath;
  Edgeband? edgeband;
  Category? category;
  ComplementaryDesign1? complementaryDesign1;
  ComplementaryDesign1? complementaryDesign2;

  ComplementaryDesign1(
      {this.id,
        this.designNo,
        this.designName,
        this.finish,
        this.title,
        this.details,
        this.hexcode,
        this.color,
        this.colorGroup,
        this.categoryId,
        this.edgebandId,
        this.complementaryDesign1Id,
        this.complementaryDesign2Id,
        this.thumbnail,
        this.imageSmall,
        this.imageMedium,
        this.imageLarge,
        this.imagePortrait,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath,
        this.imageSmallPath,
        this.imageMediumPath,
        this.imageLargePath,
        this.imagePortraitPath,
        this.edgeband,
        this.category,
        this.complementaryDesign1,
        this.complementaryDesign2});

  ComplementaryDesign1.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    designNo = json['design_no'];
    designName = json['design_name'];
    finish = json['finish'];
    title = json['title'];
    details = json['details'].toString();
    hexcode = json['hexcode'];
    color = json['color'];
    colorGroup = json['color_group'];
    categoryId = json['category_id'];
    edgebandId = json['edgeband_id'];
    complementaryDesign1Id = json['complementary_design_1_id'];
    complementaryDesign2Id = json['complementary_design_2_id'];
    thumbnail = json['thumbnail'];
    imageSmall = json['image_small'];
    imageMedium = json['image_medium'];
    imageLarge = json['image_large'];
    imagePortrait = json['image_portrait'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
    imageSmallPath = json['image_small_path'];
    imageMediumPath = json['image_medium_path'];
    imageLargePath = json['image_large_path'];
    imagePortraitPath = json['image_portrait_path'];
    edgeband = json['edgeband'] != null
        ?  Edgeband.fromJson(json['edgeband'])
        : null;
    category = json['category'] != null
        ?  Category.fromJson(json['category'])
        : null;
    complementaryDesign1 = json['complementary_design1'] != null
        ?  ComplementaryDesign1.fromJson(json['complementary_design1'])
        : null;
    complementaryDesign2 = json['complementary_design2'] != null
        ?  ComplementaryDesign1.fromJson(json['complementary_design2'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['design_no'] = designNo;
    data['design_name'] = designName;
    data['finish'] = finish;
    data['title'] = title;
    data['details'] = details;
    data['hexcode'] = hexcode;
    data['color'] = color;
    data['color_group'] = colorGroup;
    data['category_id'] = categoryId;
    data['edgeband_id'] = edgebandId;
    data['complementary_design_1_id'] = complementaryDesign1Id;
    data['complementary_design_2_id'] = complementaryDesign2Id;
    data['thumbnail'] = thumbnail;
    data['image_small'] = imageSmall;
    data['image_medium'] = imageMedium;
    data['image_large'] = imageLarge;
    data['image_portrait'] = imagePortrait;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    data['image_small_path'] = imageSmallPath;
    data['image_medium_path'] = imageMediumPath;
    data['image_large_path'] = imageLargePath;
    data['image_portrait_path'] = imagePortraitPath;
    if (edgeband != null) {
      data['edgeband'] = edgeband!.toJson();
    }
    if (category != null) {
      data['category'] = category!.toJson();
    }
    if (complementaryDesign1 != null) {
      data['complementary_design1'] = complementaryDesign1!.toJson();
    }
    if (complementaryDesign2 != null) {
      data['complementary_design2'] = complementaryDesign2!.toJson();
    }
    return data;
  }
}

class Category {
  int? id;
  String? name;
  String? code;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;

  Category(
      {this.id,
        this.name,
        this.code,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    code = json['code'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['code'] = code;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    return data;
  }
}
//
// class ComplementaryDesign1 {
//   int? id;
//   String? designNo;
//   String? designName;
//   String? finish;
//   String? title;
//   String? details;
//   String? hexcode;
//   String? color;
//   String? colorGroup;
//   String? categoryId;
//   String? edgebandId;
//   String? complementaryDesign1Id;
//   String? complementaryDesign2Id;
//   String? thumbnail;
//   String? imageSmall;
//   String? imageMedium;
//   String? imageLarge;
//   String? imagePortrait;
//   bool? hasStatus;
//   String? statusName;
//   String? statusClass;
//   String? deletedOn;
//   String? createdOn;
//   String? updatedOn;
//   String? thumbnailPath;
//   String? imageSmallPath;
//   String? imageMediumPath;
//   String? imageLargePath;
//   String? imagePortraitPath;
//   Edgeband? edgeband;
//   Category? category;
//
//   ComplementaryDesign1(
//       {this.id,
//         this.designNo,
//         this.designName,
//         this.finish,
//         this.title,
//         this.details,
//         this.hexcode,
//         this.color,
//         this.colorGroup,
//         this.categoryId,
//         this.edgebandId,
//         this.complementaryDesign1Id,
//         this.complementaryDesign2Id,
//         this.thumbnail,
//         this.imageSmall,
//         this.imageMedium,
//         this.imageLarge,
//         this.imagePortrait,
//         this.hasStatus,
//         this.statusName,
//         this.statusClass,
//         this.deletedOn,
//         this.createdOn,
//         this.updatedOn,
//         this.thumbnailPath,
//         this.imageSmallPath,
//         this.imageMediumPath,
//         this.imageLargePath,
//         this.imagePortraitPath,
//         this.edgeband,
//         this.category});
//
//   ComplementaryDesign1.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     designNo = json['design_no'];
//     designName = json['design_name'];
//     finish = json['finish'];
//     title = json['title'];
//     details = json['details'].toString();
//     hexcode = json['hexcode'];
//     color = json['color'];
//     colorGroup = json['color_group'];
//     categoryId = json['category_id'];
//     edgebandId = json['edgeband_id'];
//     complementaryDesign1Id = json['complementary_design_1_id'];
//     complementaryDesign2Id = json['complementary_design_2_id'];
//     thumbnail = json['thumbnail'];
//     imageSmall = json['image_small'];
//     imageMedium = json['image_medium'];
//     imageLarge = json['image_large'];
//     imagePortrait = json['image_portrait'];
//     hasStatus = json['hasStatus'];
//     statusName = json['statusName'];
//     statusClass = json['statusClass'];
//     deletedOn = json['deleted_on'];
//     createdOn = json['created_on'];
//     updatedOn = json['updated_on'];
//     thumbnailPath = json['thumbnail_path'];
//     imageSmallPath = json['image_small_path'];
//     imageMediumPath = json['image_medium_path'];
//     imageLargePath = json['image_large_path'];
//     imagePortraitPath = json['image_portrait_path'];
//     edgeband = json['edgeband'] != null
//         ?  Edgeband.fromJson(json['edgeband'])
//         : null;
//     category = json['category'] != null
//         ?  Category.fromJson(json['category'])
//         : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['id'] = this.id;
//     data['design_no'] = this.designNo;
//     data['design_name'] = this.designName;
//     data['finish'] = this.finish;
//     data['title'] = this.title;
//     data['details'] = this.details;
//     data['hexcode'] = this.hexcode;
//     data['color'] = this.color;
//     data['color_group'] = this.colorGroup;
//     data['category_id'] = this.categoryId;
//     data['edgeband_id'] = this.edgebandId;
//     data['complementary_design_1_id'] = this.complementaryDesign1Id;
//     data['complementary_design_2_id'] = this.complementaryDesign2Id;
//     data['thumbnail'] = this.thumbnail;
//     data['image_small'] = this.imageSmall;
//     data['image_medium'] = this.imageMedium;
//     data['image_large'] = this.imageLarge;
//     data['image_portrait'] = this.imagePortrait;
//     data['hasStatus'] = this.hasStatus;
//     data['statusName'] = this.statusName;
//     data['statusClass'] = this.statusClass;
//     data['deleted_on'] = this.deletedOn;
//     data['created_on'] = this.createdOn;
//     data['updated_on'] = this.updatedOn;
//     data['thumbnail_path'] = this.thumbnailPath;
//     data['image_small_path'] = this.imageSmallPath;
//     data['image_medium_path'] = this.imageMediumPath;
//     data['image_large_path'] = this.imageLargePath;
//     data['image_portrait_path'] = this.imagePortraitPath;
//     if (this.edgeband != null) {
//       data['edgeband'] = this.edgeband!.toJson();
//     }
//     if (this.category != null) {
//       data['category'] = this.category!.toJson();
//     }
//     return data;
//   }
// }

// class Edgeband {
//   int? id;
//   String? name;
//   bool? hasStatus;
//   String? statusName;
//   String? statusClass;
//   String? deletedOn;
//   String? createdOn;
//   String? updatedOn;
//   String? thumbnailPath;
//   String? imageSmallPath;
//   String? imageMediumPath;
//   String? imageLargePath;
//
//   Edgeband(
//       {this.id,
//         this.name,
//         this.hasStatus,
//         this.statusName,
//         this.statusClass,
//         this.deletedOn,
//         this.createdOn,
//         this.updatedOn,
//         this.thumbnailPath,
//         this.imageSmallPath,
//         this.imageMediumPath,
//         this.imageLargePath});
//
//   Edgeband.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     hasStatus = json['hasStatus'];
//     statusName = json['statusName'];
//     statusClass = json['statusClass'];
//     deletedOn = json['deleted_on'];
//     createdOn = json['created_on'];
//     updatedOn = json['updated_on'];
//     thumbnailPath = json['thumbnail_path'].toString();
//     imageSmallPath = json['image_small_path'].toString();
//     imageMediumPath = json['image_medium_path'].toString();
//     imageLargePath = json['image_large_path'].toString();
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['hasStatus'] = this.hasStatus;
//     data['statusName'] = this.statusName;
//     data['statusClass'] = this.statusClass;
//     data['deleted_on'] = this.deletedOn;
//     data['created_on'] = this.createdOn;
//     data['updated_on'] = this.updatedOn;
//     data['thumbnail_path'] = this.thumbnailPath;
//     data['image_small_path'] = this.imageSmallPath;
//     data['image_medium_path'] = this.imageMediumPath;
//     data['image_large_path'] = this.imageLargePath;
//     return data;
//   }
// }
//
// class Category {
//   int? id;
//   String? name;
//   bool? hasStatus;
//   String? statusName;
//   String? statusClass;
//   String? deletedOn;
//   String? createdOn;
//   String? updatedOn;
//
//   Category(
//       {this.id,
//         this.name,
//         this.hasStatus,
//         this.statusName,
//         this.statusClass,
//         this.deletedOn,
//         this.createdOn,
//         this.updatedOn});
//
//   Category.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     hasStatus = json['hasStatus'];
//     statusName = json['statusName'];
//     statusClass = json['statusClass'];
//     deletedOn = json['deleted_on'];
//     createdOn = json['created_on'];
//     updatedOn = json['updated_on'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data =  Map<String, dynamic>();
//     data['id'] = id;
//     data['name'] = name;
//     data['hasStatus'] = hasStatus;
//     data['statusName'] = statusName;
//     data['statusClass'] = statusClass;
//     data['deleted_on'] = deletedOn;
//     data['created_on'] = createdOn;
//     data['updated_on'] = updatedOn;
//     return data;
//   }
// }

class ComplementaryDesign2 {
  int? id;
  String? designNo;
  String? designName;
  String? finish;
  String? title;
  String? details;
  String? hexcode;
  String? color;
  String? colorGroup;
  String? categoryId;
  String? edgebandId;
  String? complementaryDesign1Id;
  String? complementaryDesign2Id;
  String? thumbnail;
  String? imageSmall;
  String? imageMedium;
  String? imageLarge;
  String? imagePortrait;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  String? thumbnailPath;
  String? imageSmallPath;
  String? imageMediumPath;
  String? imageLargePath;
  String? imagePortraitPath;
  Edgeband? edgeband;
  Category? category;

  ComplementaryDesign2(
      {this.id,
        this.designNo,
        this.designName,
        this.finish,
        this.title,
        this.details,
        this.hexcode,
        this.color,
        this.colorGroup,
        this.categoryId,
        this.edgebandId,
        this.complementaryDesign1Id,
        this.complementaryDesign2Id,
        this.thumbnail,
        this.imageSmall,
        this.imageMedium,
        this.imageLarge,
        this.imagePortrait,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.thumbnailPath,
        this.imageSmallPath,
        this.imageMediumPath,
        this.imageLargePath,
        this.imagePortraitPath,
        this.edgeband,
        this.category});

  ComplementaryDesign2.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    designNo = json['design_no'];
    designName = json['design_name'];
    finish = json['finish'];
    title = json['title'];
    details = json['details'].toString();
    hexcode = json['hexcode'];
    color = json['color'];
    colorGroup = json['color_group'];
    categoryId = json['category_id'];
    edgebandId = json['edgeband_id'];
    complementaryDesign1Id = json['complementary_design_1_id'];
    complementaryDesign2Id = json['complementary_design_2_id'].toString();
    thumbnail = json['thumbnail'];
    imageSmall = json['image_small'];
    imageMedium = json['image_medium'];
    imageLarge = json['image_large'];
    imagePortrait = json['image_portrait'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    thumbnailPath = json['thumbnail_path'];
    imageSmallPath = json['image_small_path'];
    imageMediumPath = json['image_medium_path'];
    imageLargePath = json['image_large_path'];
    imagePortraitPath = json['image_portrait_path'];
    edgeband = json['edgeband'] != null
        ?  Edgeband.fromJson(json['edgeband'])
        : null;
    category = json['category'] != null
        ?  Category.fromJson(json['category'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['design_no'] = designNo;
    data['design_name'] = designName;
    data['finish'] = finish;
    data['title'] = title;
    data['details'] = details;
    data['hexcode'] = hexcode;
    data['color'] = color;
    data['color_group'] = colorGroup;
    data['category_id'] = categoryId;
    data['edgeband_id'] = edgebandId;
    data['complementary_design_1_id'] = complementaryDesign1Id;
    data['complementary_design_2_id'] = complementaryDesign2Id;
    data['thumbnail'] = thumbnail;
    data['image_small'] = imageSmall;
    data['image_medium'] = imageMedium;
    data['image_large'] = imageLarge;
    data['image_portrait'] = imagePortrait;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    data['thumbnail_path'] = thumbnailPath;
    data['image_small_path'] = imageSmallPath;
    data['image_medium_path'] = imageMediumPath;
    data['image_large_path'] = imageLargePath;
    data['image_portrait_path'] = imagePortraitPath;
    if (edgeband != null) {
      data['edgeband'] = edgeband!.toJson();
    }
    if (category != null) {
      data['category'] = category!.toJson();
    }
    return data;
  }
}
