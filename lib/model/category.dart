
class CategoryData {
  int? id;
  String? code;
  String? name;
  String? details;
  String? remarks;
  String? parentId;
  String? status;
  String? createdAt;
  String? updatedAt;
  String? createdBy;
  String? updatedBy;
  String? deletedAt;
  String? designsCount;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;

  CategoryData(
      {this.id,
        this.code,
        this.name,
        this.details,
        this.remarks,
        this.parentId,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.createdBy,
        this.updatedBy,
        this.deletedAt,
        this.designsCount,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn});

  CategoryData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    details = json['details'];
    remarks = json['remarks'];
    parentId = json['parent_id'].toString();
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    deletedAt = json['deleted_at'];
    designsCount = json['designs_count'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['name'] = name;
    data['details'] = details;
    data['remarks'] = remarks;
    data['parent_id'] = parentId;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['deleted_at'] = deletedAt;
    data['designs_count'] = designsCount;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    return data;
  }
}
