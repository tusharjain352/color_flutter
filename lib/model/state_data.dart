class StateData {
  int? id;
  String? name;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;
  List<Cities>? cities;

  StateData(
      {this.id,
        this.name,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn,
        this.cities});

  StateData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    if (json['cities'] != null) {
      cities = <Cities>[];
      json['cities'].forEach((v) {
        cities!.add(Cities.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    if (cities != null) {
      data['cities'] = cities!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Cities {
  int? id;
  String? name;
  String? pincode;
  String? stateId;
  String? remarks;
  String? status;
  String? createdAt;
  String? updatedAt;
  String? deletedAt;
  String? createdBy;
  String? updatedBy;
  bool? hasStatus;
  String? statusName;
  String? statusClass;
  String? deletedOn;
  String? createdOn;
  String? updatedOn;

  Cities(
      {this.id,
        this.name,
        this.pincode,
        this.stateId,
        this.remarks,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.createdBy,
        this.updatedBy,
        this.hasStatus,
        this.statusName,
        this.statusClass,
        this.deletedOn,
        this.createdOn,
        this.updatedOn});

  Cities.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    pincode = json['pincode'];
    stateId = json['state_id'];
    remarks = json['remarks'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    createdBy = json['created_by'];
    updatedBy = json['updated_by'];
    hasStatus = json['hasStatus'];
    statusName = json['statusName'];
    statusClass = json['statusClass'];
    deletedOn = json['deleted_on'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['pincode'] = pincode;
    data['state_id'] = stateId;
    data['remarks'] = remarks;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['deleted_at'] = deletedAt;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['hasStatus'] = hasStatus;
    data['statusName'] = statusName;
    data['statusClass'] = statusClass;
    data['deleted_on'] = deletedOn;
    data['created_on'] = createdOn;
    data['updated_on'] = updatedOn;
    return data;
  }
}
