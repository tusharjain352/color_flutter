import 'package:flutter/material.dart';

class DrawerOption {
  String? label;
  String? title;
  Widget? child;

  Function(BuildContext context)? onPress;

  DrawerOption({
    this.label,
    this.title,
    this.child,
    this.onPress,
  });
}
