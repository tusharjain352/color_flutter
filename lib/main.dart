import 'package:camera/camera.dart';
import 'package:colurama/model/catalogue.dart';
import 'package:colurama/model/country.dart';
import 'package:colurama/model/user.dart';
import 'package:colurama/view/screens/login_screen_view.dart';
import 'package:colurama/view_model/catalogue_view_model.dart';
import 'package:colurama/view_model/contact_us_view_model.dart';
import 'package:colurama/view_model/country_view_model.dart';
import 'package:colurama/view_model/drawer_view_model.dart';
import 'package:colurama/view_model/feedback_view_model.dart';
import 'package:colurama/view_model/home_view_model.dart';
import 'package:colurama/view_model/inspiration_view_model.dart';
import 'package:colurama/view_model/login_view_model.dart';
import 'package:colurama/view_model/select_shade_vm.dart';
import 'package:colurama/view_model/shade_designs_view_model.dart';
import 'package:colurama/view_model/wishlist_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:colurama/app_manager/constant/project_constant.dart';
import 'package:colurama/app_manager/theme/theme_provider.dart';
import 'package:colurama/authentication/user_repository.dart';

import 'view_model/enquire_view_model.dart';
import 'view_model/select_design_view_model.dart';

class NavigationService {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static BuildContext? get context => navigatorKey.currentContext;
}




Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  bool isLightTheme = await ThemeProvider().retrieveStoredTheme();
  List<CameraDescription> cameras = await availableCameras();
  User user = await UserRepository.fetchUserData();
  Country currentCountry = await CountryVM.fetchCurrentCountry();
  List<Design> wishList = await WishListViewModel.fetWishList();

  runApp(MultiProvider(providers: [
    ChangeNotifierProvider<ThemeProvider>(
      create: (_) => ThemeProvider(isLightTheme: isLightTheme),
    ),
    ChangeNotifierProvider<UserRepository>(
      create: (_) => UserRepository(currentUser: user),
    ),
    ChangeNotifierProvider<LoginVM>(
      create: (_) => LoginVM(),
    ),
    ChangeNotifierProvider<HomeVM>(
      create: (_) => HomeVM(),
    ),
    ChangeNotifierProvider<CountryVM>(
      create: (_) => CountryVM(currentCountry: currentCountry),
    ),
    ChangeNotifierProvider<SelectShadeVM>(
      create: (_) => SelectShadeVM(
        cameras: cameras
      ),
    ),
    ChangeNotifierProvider<ShadeDesignsVM>(
      create: (_) => ShadeDesignsVM(),
    ),
    ChangeNotifierProvider<InspirationVM>(
      create: (_) => InspirationVM(),
    ),
    ChangeNotifierProvider<ContactUsVM>(
      create: (_) => ContactUsVM(),
    ),
    ChangeNotifierProvider<FeedbackVM>(
      create: (_) => FeedbackVM(),
    ),
    ChangeNotifierProvider<CatalogueVM>(
      create: (_) => CatalogueVM(),
    ),
    ChangeNotifierProvider<SelectDesignViewModel>(
      create: (_) => SelectDesignViewModel(),
    ),
    ChangeNotifierProvider<WishListViewModel>(
      create: (_) => WishListViewModel(
        wishList: wishList
      ),
    ),
    ChangeNotifierProvider<EnquireViewModel>(
      create: (_) => EnquireViewModel(),
    ),
  ], child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);
    Widget initialWidget =
        UserRepository.of(context).getUser.id.toString() == "null"
            ? const LoginScreenView()
            : DrawerVM.homeRoute;

    //  initialWidget=const TestScreen();

    return MaterialApp(
      navigatorKey: NavigationService.navigatorKey,
      theme: themeProvider.getThemeData,
      title: ProjectConstant.name,
      debugShowCheckedModeBanner: false,
      //  home: const HomeView(),
      home: initialWidget,
    );
  }
}
