import 'package:colurama/model/catalogue.dart';
import 'package:colurama/view/screens/drawer_screens/design_view/design_view.dart';
import 'package:colurama/view/screens/home_screen_view.dart';
import 'package:colurama/view/screens/shade_selecter/image_shade_view.dart';
import 'package:colurama/view/screens/shade_selecter/select_shade_view.dart';
import 'package:colurama/view/screens/shade_selecter/shade_design_view.dart';
import 'package:colurama/view/widget/drawered_view.dart';
import 'package:flutter/material.dart';

class Routes {


  static const selectShade= DraweredView(
    title: "SELECT A SHADE",
    child: SelectShadeView(),
  );

  static const home= DraweredView(
    title: "HOME",
    child: HomeScreenView(),
  );

  static const imageShade= DraweredView(
    title: "IMAGE SHADE",
    child: ImageShadeView(),
  );

  static Widget shadeDesigns(String shadeHex) {
    return  DraweredView(
      title: "SHADE DESIGNS",
      child: ShadeDesignView(
        shadeHexCode: shadeHex,
      ),
    );
  }

  static Widget designDetails(Design design) {
    return  DraweredView(
      title: design.title??"",
      child: DesignView(
        design: design,
      ),
    );
  }



}